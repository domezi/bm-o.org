import React, { Component } from 'react';
import { DataTable, Column } from 'primereact/datatable';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import { customerSex } from '../Api'
import { Dropdown } from 'primereact/dropdown';

export class CustomersPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dialogVisible: false,
            confirmVisible: false,
            confirmMessage: "Möchten Sie fortfahren?",
            confirmFun: () => { console.log("not implemented") },
            searchPattern:"",
            isSearch:true,
            currentFields: { title: "", sex: "", address1: "", address2: "", email: "", phone: "", iss: "", fname: "", lname: "", uid: 0 },
            currentAction: "insert",
        };
    }

    onFieldChange(e) {
        if(e.target.name === "searchPattern") {
            this.setState({searchPattern: e.target.value}) 
        } else {
            let currentFields = this.state.currentFields
            currentFields[e.target.name] = e.target.value
            this.setState({ currentFields })
        }
    }

    showSuccess(detail) {
        this.growl.show({ closable: true, severity: 'success', summary: 'Erfolgreich druchgeführt!', detail });
    }

    showError(detail) {
        this.growl.show({ closable: true, severity: 'error', summary: 'Fehler!', detail });
    }

    addCustomerBtn() {
      if (this.state.currentAction === "insert") {
        // add the customer
        this.props.api.insertCustomer(this.state.currentFields)
            .then((r) => {
                return r.json()
            })
            .then((r) => {
                setTimeout(()=>{this.props.createProjectWithCustomer(r.insert_id)},800);
                this.setState({ dialogVisible: false })
                this.showSuccess("Gespeichert.")
                this.props.refreshCustomers()
            })
            .catch((e) => {
                this.showError("Konnte nicht erstellt werden.")
                })
        } else {
            // update the customer
            this.props.api.updateCustomer(this.state.currentFields)
                .then((r) => {
                    this.setState({ dialogVisible: false })
                    this.showSuccess("Änderungen gespeichert.")
                    this.props.refreshCustomers()
                })
                .catch((e) => {
                    this.showError("Speichern der Änderungen fehlgeschlagen.")
                }
                )}

    }


    render() {
        return (
            <div className="p-grid">
                <div className="p-col-12">
                    <div className="card">
                        <Growl ref={(el) => this.growl = el} position="topright"></Growl>

                        <h1>Kunden</h1>
                        <div className="header-actions">
                            <Button icon="pi pi-search" className={this.state.isSearch ? "" : "notactive"} onClick={() => { this.setState({ searchPattern: "", isSearch : !this.state.isSearch }) }} label="" />
                            <Button disabled={!this.props.api.hasPolicy("customers_create")} icon="pi pi-plus" onClick={() => { this.setState({ currentAction: "insert", currentFields: { title: "", sex: "", address1: "", address2: "", email: "", phone: "", iss: "", fname: "", lname: "", uid: 0 }, dialogVisible: true }) }} label="Hinzufügen" />
                        </div>

                        <Dialog style={{ textAlign: "center" }} header="Fortfahren?" visible={this.state.confirmVisible} modal={true} onHide={() => this.setState({ confirmVisible: false })}
                            footer={<div>
                                <Button onClick={() => { this.setState({ confirmVisible: false }) }} label="Abbrechen" className="p-button-secondary" icon="pi pi-times" />
                                <Button onClick={() => { this.state.confirmFun(); this.setState({ confirmVisible: false }) }} label="Fortfahren" className="p-button-primary" icon="pi pi-check" />
                            </div>} >{this.state.confirmMessage}</Dialog>

                        <Dialog header={
                            this.state.currentAction === "insert" ?
                                "Hinzufügen" : "Bearbeiten"
                        } visible={this.state.dialogVisible} modal={true} onHide={() => this.setState({ dialogVisible: false })}
                            footer={<div>
                                <Button onClick={() => { this.setState({ dialogVisible: false }) }} label="Abbrechen" className="p-button-secondary" icon="pi pi-times" />
                                <Button onClick={() => { this.addCustomerBtn() }} label={this.state.currentAction === "insert" ? "Hinzufügen" : "Speichern"} icon="pi pi-check" />
                                { this.state.currentAction === "insert" && <Button className="fullblock" onClick={() => { this.addCustomerBtn() }} label="Hinzufügen & Projekt anlegen" icon="pi pi-plus" /> }
                            </div>}
                            className="form"
                        >
                            <div>
                                <Dropdown name="sex" value={this.state.currentFields.sex} options={customerSex} onChange={this.onFieldChange.bind(this)} placeholder="Anrede" />
                                <InputText name="title" value={this.state.currentFields.title} onChange={this.onFieldChange.bind(this)} placeholder={`Titel (z.b: "Dr. ","Dr. Prof.") [mit Leerzeichen endend]`} />
                                <InputText name="fname" value={this.state.currentFields.fname} onChange={this.onFieldChange.bind(this)} placeholder="Vorname" />
                                <InputText name="lname" value={this.state.currentFields.lname} onChange={this.onFieldChange.bind(this)} placeholder="Nachname" />
                                <InputText name="address1" value={this.state.currentFields.address1} onChange={this.onFieldChange.bind(this)} placeholder="Strasse & Hausnummer" />
                                <InputText name="address2" value={this.state.currentFields.address2} onChange={this.onFieldChange.bind(this)} placeholder="Ort & PLZ" />
                                <InputText name="email" value={this.state.currentFields.email} onChange={this.onFieldChange.bind(this)} placeholder="E-mail" />
                                <InputText name="phone" value={this.state.currentFields.phone} onChange={this.onFieldChange.bind(this)} placeholder="Telefonnummer" />
                            </div>
                        </Dialog>

                        { this.props.customers.length > 0 && this.state.isSearch && <InputText name="searchPattern" onKeyUp={(e)=>{if(e.keyCode === 27) this.setState({searchPattern:""}); if(e.keyCode === 13 || e.keyCode === 27) this.setState({isSearch:false})}} value={this.state.searchPattern} onChange={this.onFieldChange.bind(this)} placeholder="Suchen nach ..." autoFocus={true} /> }

                        {
                            this.props.customers.length > 0 ?
                                <DataTable value={this.props.customers.slice().reverse().filter((o)=>{
                                    return (this.state.searchPattern === "" || o.fname.toLowerCase().includes(this.state.searchPattern.toLowerCase()) || o.lname.toLowerCase().includes(this.state.searchPattern.toLowerCase()))
                                })}>
                                    <Column field={"lname"} header={"Name"} body={(rowData, column)=>{return rowData.fname+" "+rowData.lname;}} />
                                    <Column field={"phone"} header={"Telefonnummer"}  body={(row,email)=>{return <div><a href={"tel:"+row.phone}>{row.phone}</a></div>}} />
                                    <Column field={"email"} header={"E-mail"} body={(row,email)=>{return <div><a href={"mailto:"+row.email}>{row.email}</a></div>}} />
                                    <Column field={"uid"} header={"Ansprechpartner*in"} body={(row,col)=>{
                                        return this.props.users.map(user => {
                                            if(user.id == row.uid)
                                                return user.fname+" "+user.lname;
                                        }) 
                                    }} />
                                    <Column style={{ width: "8em", textAlign: "center" }} body={(rowData, column) => {
                                        return <div>
                                            <Button disabled={!this.props.api.hasPolicy("customers_edit")} onClick={() => {
                                                this.setState({ dialogVisible: true, currentAction: "edit", currentFields: rowData }, () => { console.log(this.state) })
                                            }} type="button" icon="pi pi-pencil" className="p-button-secondary"></Button>
                                            <Button disabled={!this.props.api.hasPolicy("customers_delete")} onClick={() => {
                                                this.setState({
                                                    confirmVisible: true,
                                                    confirmMessage: "Kunde*in " + rowData.fname + " " + rowData.lname + " löschen?",
                                                    confirmFun: () => {
                                                        this.props.api.deleteCustomer(rowData.id)
                                                            .then(r => {
                                                                if (r.status == 200) {
                                                                    this.props.refreshCustomers()
                                                                    this.showSuccess("Wurde erfolgreich gelöscht.")
                                                                } else if (r.status == 420) {
                                                                    this.showError("Du darfst dieses Element nicht löschen.")
                                                                } else {
                                                                    this.showError("Es ist ein Fehler aufgetreten.")
                                                                }
                                                            })
                                                            .catch(r => this.showError("Konnte nicht gelöscht werden."))
                                                    }
                                                })
                                            }} type="button" style={{ marginLeft: 10 }} icon="pi pi-trash" className="p-button-danger"></Button>
                                        </div>
                                    }} />
                                </DataTable>
                                :
                                <p>Keine Elemente gefunden.</p>
                        }

                    </div>
                </div>
            </div>
        );
    }
}
