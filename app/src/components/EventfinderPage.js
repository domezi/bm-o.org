import React, { Component } from 'react';
import { DataTable, Column } from 'primereact/datatable';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import { Dropdown } from 'primereact/dropdown';
import { InputTextarea } from 'primereact/inputtextarea';
import moment from 'moment'
import 'moment/locale/de'  // without this line it didn't work

class ReadMoreDiv extends Component {
    constructor(props) {
        super(props)
        this.state = { readMoreEnabled : false }
    }
    render() {
        if(this.props.text.split("\n").length < 2)
            return this.props.text
        return <div>
            <i style={{cursor:"pointer",float:"right"}} className={"pi pi-chevron-"+(!this.state.readMoreEnabled ? "down" : "up")} onClick={()=>this.setState({readMoreEnabled:!this.state.readMoreEnabled})}/>
            {
                this.state.readMoreEnabled ? this.props.text : this.props.text.split('\n')[0]
            }
        </div>
    }
}

export class EventfinderPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dialogVisible: false,
            confirmVisible: false,
            confirmMessage: "Möchten Sie fortfahren?",
            confirmFun: () => { console.log("not implemented") },
            currentFields: { text: "", mins: 0, euros: 0, tid: 0 },
            currentTaskFields: { title: "", mins: 0, assignee_uid: 0, euros: 0, uid: 0, deadline: "", description: "" },
            currentAction: "insert",

            //hours: this.props.hours,
            //tasks: [],
            //wages: [],
            //pid: 0
        };
    }

    /*
    refreshWages() {
        // TODO remove
        return
        this.props.api.getWages(this.props.pid)
            .then(r => r.json())
            .then(r => this.setState({ wages: r.data }))
            .catch(e => console.log(e))
    }

    refreshHours() {
        // TODO remove
        return
        this.setState({ pid: this.props.pid }, () => {
            this.props.api.getHours(this.props.pid)
                .then(d => d.json())
                .then(d => {
                    this.setState({ hours: d.data, tasks: d.tasks })
                })
                .catch(e => this.showError("Konnte Stunden/Tasks nicht aktualisieren."))
            this.refreshWages()
        })
    }
*/

    getWage(mins, uid) {
        for (let wage of this.props.wages) {
            if (parseInt(wage.uid) === parseInt(uid) && parseInt(wage.pid) === parseInt(this.props.pid)) {
                return (mins * parseInt(wage.wage) / 60).toFixed(2)
            }
        }
        return 0
    }

    onTaskFieldChange(e) {
        let currentTaskFields = this.state.currentTaskFields
        if (e.target.name === "mins" && this.state.currentAction === "task_create") {
            e.target.value = parseInt("0" + e.target.value)
            currentTaskFields.euros = this.getWage(e.target.value, currentTaskFields.assignee_uid)
        }
        currentTaskFields[e.target.name] = e.target.value
        this.setState({ currentTaskFields })
    }

    onFieldChange(e) {
        let currentFields = this.state.currentFields
        if (e.target.name === "mins" && this.state.currentAction === "insert") {
            e.target.value = parseInt("0" + e.target.value)
            currentFields.euros = this.getWage(e.target.value, this.props.api.getUserfield("id")) // TODO replace own
        }
        currentFields[e.target.name] = e.target.value
        this.setState({ currentFields })
    }
    showSuccess(detail) {
        this.growl.show({ closable: true, severity: 'success', summary: 'Erfolgreich druchgeführt!', detail });
    }

    showError(detail) {
        if(this.growl !== null)
            this.growl.show({ closable: true, severity: 'error', summary: 'Fehler!', detail });
        else 
            console.error("couldnt show error: "+detail)
    }

    render() {
        if (this.props.pid !== this.props.pid)
            this.refreshHours()
        return (
            <div className="p-grid">
                <div className="p-col-12">
                    
                    <div className="card">

                        <h1>Termine finden</h1>
                        <div className="header-actions">
                            <Button disabled={!this.props.api.hasPolicy("hours_create")} icon="pi pi-plus" onClick={() => { this.setState({ currentAction: "insert", currentFields: { pid: this.props.pid, text: "", mins: 0, euros: 0, tid: 0 }, dialogVisible: true }) }} label="Hinzufügen" />
                        </div>
                        <div className="subtitle">Finde einen Termin, an dem ihr alle Zeit habt.</div>

                        <i>Wird bald implementiert.</i>

                    </div>
                </div>
            </div >
        );
    }
}
