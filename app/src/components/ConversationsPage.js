import React, { Component } from 'react';
import { DataTable, Column } from 'primereact/datatable';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Growl } from 'primereact/growl';
import { Dropdown } from 'primereact/dropdown';
import { InputTextarea } from 'primereact/inputtextarea';
import { Link } from 'react-router-dom'
import moment from 'moment'
import 'moment/locale/de'  // without this line it didn't work

class ReadMoreDiv extends Component {
    constructor(props) {
        super(props)
        this.state = { readMoreEnabled : true }
    }
    render() {
        if(this.props.text.split("\n").length < 2)
            return this.props.text
        return <div className="firstline-bold">
            <i style={{cursor:"pointer",float:"right"}} className={"pi pi-chevron-"+(!this.state.readMoreEnabled ? "down" : "up")} onClick={()=>this.setState({readMoreEnabled:!this.state.readMoreEnabled})}/>
            {
                this.state.readMoreEnabled ? this.props.text : this.props.text.split('\n')[0]
            }
        </div>
    }
}

export class ConversationsPage extends Component {

    componentDidMount() {
    }

    constructor(props) {
        super(props);
        this.textarea = null
        this.state = {
            dialogVisible: false,
            confirmVisible: false,
            confirmMessage: "Möchten Sie fortfahren?",
            confirmFun: () => { console.log("not implemented") },
            currentFields: { text: "Kundenkontakt Notizen\n", mins: 0, euros: 0, tid: 0 },
            currentTaskFields: { title: "", mins: 0, assignee_uid: 0, euros: 0, uid: 0, deadline: "", description: "" },
            currentAction: "insert",
            /*
            hours: [],
            tasks: [],
            wages: [],
            pid: 0
            */
        };
    }

    refreshWages() {
        //TODO remove
        return
        this.props.api.getWages(this.props.pid)
            .then(r => r.json())
            .then(r => this.setState({ wages: r.data }))
            .catch(e => console.log(e))
    }

    refreshHours() { 
        //TODO remove
        return
        this.setState({ pid: this.props.pid }, () => {
            this.props.api.getHours(this.props.pid)
                .then(d => d.json())
                .then(d => {
                    console.log(d.data)
                    this.setState({ hours: d.data.filter(hours=>(hours.euros === "0" && hours.mins === "0") || hours.text.includes("elefon") || hours.text.includes("undenkontakt")), tasks: d.tasks })
                })
                .catch(e => this.showError("Konnte Kundenkontakte nicht aktualisieren."))
            this.refreshWages()
        })
    }

    getWage(mins, uid) {
        console.log(this.props.wages)
        for (let wage of this.props.wages) {
            console.log("checking wage agains uid:" + wage.uid + " vs " + uid)
            if (parseInt(wage.uid) === parseInt(uid) && parseInt(wage.pid) === parseInt(this.props.pid))
                return (mins * parseInt(wage.wage) / 60).toFixed(2)
        }
        return 0
    }

    onTaskFieldChange(e) {
        console.log(this.props.wages)
        let currentTaskFields = this.state.currentTaskFields
        if (e.target.name === "mins" && this.state.currentAction === "task_create") {
            e.target.value = parseInt("0" + e.target.value)
            currentTaskFields.euros = this.getWage(e.target.value, currentTaskFields.assignee_uid)
        }
        currentTaskFields[e.target.name] = e.target.value
        this.setState({ currentTaskFields })
    }

    onFieldChange(e) {
        let currentFields = this.state.currentFields
        if (e.target.name === "mins" && this.state.currentAction === "insert") {
            e.target.value = parseInt("0" + e.target.value)
            currentFields.euros = this.getWage(e.target.value, this.props.api.getUserfield("id")) // TODO replace own
        }
        currentFields[e.target.name] = e.target.value
        this.setState({ currentFields })
    }
    showSuccess(detail) {
        if(this.growl !== undefined)
            this.growl.show({ closable: true, severity: 'success', summary: 'Erfolgreich druchgeführt!', detail });
    }

    showError(detail) {
        if(this.growl !== undefined)
            this.growl.show({ closable: true, severity: 'error', summary: 'Fehler!', detail });
        else 
            console.error("couldnt show error: "+detail)
    }

    render() {
        /*
        if (this.props.pid !== this.props.pid)
            this.refreshHours()
            */
        return (
            <div className="p-grid">
                <div className="p-col-12">
                    <div className="card">

                        <Growl ref={(el) => this.growl = el} position="topright"></Growl>

                        <h1>Kundenkontakt</h1>
                        <div className="header-actions">
                            <Button disabled={!this.props.api.hasPolicy("hours_create")} icon="pi pi-plus" onClick={() => { this.setState({ currentAction: "insert", currentFields: { pid: this.props.pid, mins: 0, euros: 0, tid: 0 }, dialogVisible: true }) }} label="Hinzufügen" />
                        </div>

                        <div className="subtitle">Dies ist eine reduzierte Darstellung <Link to="/hours">aller Tätigkeiten</Link>.<br/>
                            Als Kundenkontakt zählen alle Tätigkeiten, die die Stichworte "Telefonieren" oder "Kundenkontakt" enthalten. Tätigkeiten ohne entstandene Kosten bzw. Zeit zählen ebenfalls als Kundenkontakt.</div>

                        <Dialog style={{ textAlign: "center" }} header="Fortfahren?" visible={this.state.confirmVisible} modal={true} onHide={() => this.setState({ confirmVisible: false })}
                            footer={<div>
                                <Button onClick={() => { this.setState({ confirmVisible: false }) }} label="Abbrechen" className="p-button-secondary" icon="pi pi-times" />
                                <Button onClick={() => { this.state.confirmFun(); this.setState({ confirmVisible: false }) }} label="Fortfahren" className="p-button-primary" icon="pi pi-check" />
                            </div>} >{this.state.confirmMessage}</Dialog>

                        <Dialog header={
                            this.state.currentAction === "insert" ? "Tätigkeit registrieren" :
                                this.state.currentAction === "task_create" ?
                                    "Aufgabe anlegen" : "Bearbeiten"
                        } visible={this.state.dialogVisible} modal={true} onHide={() => this.setState({ dialogVisible: false })}
                            footer={<div>
                                <Button onClick={() => { this.setState({ dialogVisible: false }) }} label="Abbrechen" className="p-button-secondary" icon="pi pi-times" />
                                <Button onClick={() => {
                                    if (this.state.currentAction === "insert") {
                                        // add the hour
                                        this.props.api.insertHour(this.props.pid, this.state.currentFields)
                                            .then((r) => {
                                                this.setState({ dialogVisible: false, currentFields: {...this.state.currentFields, text:"Kundenkontakt weitere Notizen\n"}})
                                                this.props.refreshHours()
                                                this.showSuccess("Gespeichert.")
                                            })
                                            .catch((e) => {
                                                console.log("e",e)
                                                this.showError("Tätigkeit konnte nicht erstellt werden.")
                                            })
                                    } else if (this.state.currentAction === "task_create") {
                                        // add the hour
                                        this.props.api.insertTask(this.props.pid, this.state.currentTaskFields)
                                            .then((r) => {
                                                this.setState({ dialogVisible: false })
                                                this.props.refreshHours()
                                                this.showSuccess("Gespeichert.")
                                            })
                                            .catch((e) => {
                                                console.log("e",e)
                                                this.showError("Aufgabe konnte nicht erstellt werden.")
                                            })
                                    } else {
                                        // update the hour
                                        this.props.api.updateHour(this.state.currentFields)
                                            .then((r) => {
                                                this.setState({ dialogVisible: false })
                                                this.showSuccess("Änderungen gespeichert.")
                                                this.props.refreshHours()
                                            })
                                            .catch((e) => {
                                                this.showError("Speichern der Änderungen fehlgeschlagen.")
                                            })
                                    }
                                }} label={this.state.currentAction !== "edit" ? "Hinzufügen" : "Speichern"} icon="pi pi-check" />
                            </div>}
                            className="form"
                        >
                            <div>
                               <div>
                                    <InputTextarea autoFocus={true} style={{width:"80vw",height:"50vh"}} autoFocus={true} name="text" value={this.state.currentFields.text} onChange={this.onFieldChange.bind(this)} placeholder={`Tätigkeit beschreiben`} />
                                  <div className="p-inputgroup">
                                                <InputText name="mins" value={this.state.currentFields.mins} onChange={this.onFieldChange.bind(this)} placeholder="Minuten" />
                                                <span className="p-inputgroup-addon">min</span>
                                            </div>
                                            <div className="p-inputgroup">
                                                <InputText name="euros" value={this.state.currentFields.euros} onChange={this.onFieldChange.bind(this)} placeholder="Betrag in Euro" />
                                                <span className="p-inputgroup-addon">€</span>
                                            </div>
                                                <Dropdown name="tid" options={this.props.tasks.map((task) => {
                                        return { label: task.title, value: task.id }
                                    })} value={this.state.currentFields.tid} onChange={this.onFieldChange.bind(this)} placeholder="Zugeordnete Aufgabe" />
                                </div>
                            </div>
                        </Dialog>

                        {
                            this.props.hours.length > 0 ?
                                <DataTable value={this.props.hours}>

                                     <Column field={"iss"} style={{width:"10em"}} header={"Wann"} body={(r,c)=>{

                                        var now = moment(new Date()); //todays date
                                        var end = moment(r.iss); // another date

                                        var duration = moment.duration(-now.diff(end));

                                        moment.locale('de')

                                        return duration.humanize(true)
                                    }}/>

                                    <Column style={{"white-space":"pre-line"}} field={"text"} header={"Tätigkeit"} body={(r,c)=>{
                                        return <ReadMoreDiv text={r.text}/>
                                    }}/>
                                    <Column field={"tid"} header={"Aufgabe"} body={(rowData) => {
                                        for (let task of this.props.tasks) {
                                            if (rowData.tid === task.id)
                                                return <span>{task.title}<div style={{opacity:.5}}>{task.description}</div></span>
                                        }
                                    }} />
                                    <Column style={{ width: "10em" }} field={"uid"} header={"Benutzer"} body={(rowData, column) => {
                                        for (let user of this.props.users) {
                                            if (user.id === rowData.uid)
                                                return user.fname
                                        }
                                        return "- - -"
                                    }} />
                                        
                                    <Column style={{ width: "8em", textAlign: "center" }} body={(rowData, column) => {
                                        return <div>
                                            <Button disabled={!this.props.api.hasPolicy("hours_edit") && !( rowData.uid === this.props.api.getUserfield("id") && rowData.ttl < 1500 ) } onClick={() => { // change if younger than 25 minutes
                                                this.setState({ dialogVisible: true, currentAction: "edit", currentFields: rowData }, () => { console.log(this.state) })
                                            }} type="button" icon="pi pi-pencil" className="p-button-secondary"></Button>
                                            <Button disabled={!this.props.api.hasPolicy("hours_delete")} onClick={() => {
                                                this.setState({
                                                    confirmVisible: true,
                                                    confirmMessage: "Tätigkeit " + rowData.text + " löschen?",
                                                    confirmFun: () => {
                                                        this.props.api.deleteHour(rowData.id)
                                                            .then(r => {
                                                                if (r.status === 200) {
                                                                    this.showSuccess("Wurde erfolgreich gelöscht.")
                                                                    this.props.refreshHours()
                                                                } else if (r.status == 420) {
                                                                    this.showError("Du darfst dieses Element nicht löschen.")
                                                                } else {
                                                                    this.showError("Es ist ein Fehler aufgetreten.")
                                                                }
                                                            })
                                                            .catch(r => this.showError("Konnte nicht gelöscht werden."))
                                                    }
                                                })
                                            }} type="button" style={{ marginLeft: 10 }} icon="pi pi-trash" className="p-button-danger"></Button>
                                        </div>
                                    }} />
                                </DataTable>
                                :
                                <p>Keine Elemente gefunden.</p>
                        }

                    </div>
                </div>
            </div >
        );
    }
}
