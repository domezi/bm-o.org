import React, { Component } from 'react';
import { InputText } from 'primereact/inputtext';
import PropTypes from 'prop-types';
import { Dropdown } from 'primereact/dropdown';
import { ProgressBar } from 'primereact/progressbar';

import { InputTextarea } from 'primereact/inputtextarea';

export class AppTopbar extends Component {

    static defaultProps = {
        onToggleMenu: null
    }

    static propTypes = {
        onToggleMenu: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props)
    }

    /*
                */

    render() {
        var projects = this.props.projects

        return (
            <div className="layout-topbar clearfix" style={{fontSize:20,padding:10,paddingLeft:20}}>
               Terminplaner
            </div>
        );
    }
}
