-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: db5000214720.hosting-data.io
-- Generation Time: Nov 09, 2019 at 09:19 AM
-- Server version: 5.7.27-log
-- PHP Version: 7.0.33-0+deb9u6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbs209467`
--
CREATE DATABASE IF NOT EXISTS `dbs209467` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `dbs209467`;

-- --------------------------------------------------------

--
-- Table structure for table `Customers`
--

CREATE TABLE `Customers` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `sex` enum('w','m','o') NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'created by uid'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Customers`
--

INSERT INTO `Customers` (`id`, `title`, `sex`, `address1`, `address2`, `email`, `phone`, `iss`, `fname`, `lname`, `uid`) VALUES
(5, 'Dr. ', '', '', '', '', '', '2019-11-08 09:16:52', 'Max', 'Mustermann', 0),
(6, '', '', '', '', '', '', '2019-11-08 19:21:18', 'Maya', 'Schenk', 6);

-- --------------------------------------------------------

--
-- Table structure for table `Files`
--

CREATE TABLE `Files` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `iss` int(11) NOT NULL,
  `type` enum('quote','invoice','other') NOT NULL,
  `uid` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `IncoiveHasFixed`
--

CREATE TABLE `IncoiveHasFixed` (
  `invoice_id` int(11) NOT NULL,
  `fixed_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `InvoiceHasHours`
--

CREATE TABLE `InvoiceHasHours` (
  `invoice_id` int(11) NOT NULL,
  `hours_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Invoices`
--

CREATE TABLE `Invoices` (
  `id` int(11) NOT NULL COMMENT 'rechnungsnr zb 1902 (2019 2nd invoice)',
  `pid` int(11) NOT NULL,
  `status` enum('awaiting_payment','paid','') NOT NULL,
  `iss` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Logins`
--

CREATE TABLE `Logins` (
  `uid` int(11) NOT NULL,
  `hash` varchar(128) NOT NULL,
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Logins`
--

INSERT INTO `Logins` (`uid`, `hash`, `iss`) VALUES
(6, '0170a70044060cf86c1d9aed8c12bdf7403fa0a92e7db03d85beab22dd9ba2a3948c224bf66098326b6ebb423eae6746db12304b2a0ebf99e9003262efb302a5', '2019-11-06 16:51:42'),
(6, '34619c4915d35802b8987c00d62d6bb29046a10f9264b63a554a857d6c904194f57b3e7b33f405ea9cce3f04963c15f6056561487c0fb81d9edee108a6ccf794', '2019-11-06 16:20:47'),
(6, '4f737f91d645d638764d86982ac274855e3353deab74b929457f7f340a12a61534279382141ee72d69aa79c5a892974ec761fe5cac8db7b1de494d8dfcf807e7', '2019-11-06 16:51:34'),
(6, '5becd5fcd13ca9fcee1842c64cd03541e9506ba99b50ee0777c71303061016b9b90bb42fecf76b00663b4826f377d63438372982d243d56b2eeb79c162d7528b', '2019-11-06 16:21:00'),
(6, 'ab2005735b20041b6eff999b2f8f14a7ea4612acc1fc3ce8621b4f4d5d2c43213add7980aaab831023a871d0554cc772ce07d4552edfe91e89654cc13c1e8258', '2019-11-06 16:51:43'),
(6, 'f50aabfd6a520022f1852b38b8d026b0d1f209aaf3766c6e27f287c70a5a386c943be3f6a538430a205dccc5627e973cb04e7974220be56a97b720dce2c94c37', '2019-11-06 16:50:43');

-- --------------------------------------------------------

--
-- Table structure for table `Policies`
--

CREATE TABLE `Policies` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `policy` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Policies`
--

INSERT INTO `Policies` (`id`, `title`, `policy`) VALUES
(1, 'Superuser', '{\"*\": \"*\"}'),
(6, 'Mitarbeiterverwaltung', '{\"users\": \"*\"}');

-- --------------------------------------------------------

--
-- Table structure for table `ProjectHasFixed`
--

CREATE TABLE `ProjectHasFixed` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `title` text NOT NULL,
  `cents` int(11) NOT NULL,
  `status` enum('waiting','awaiting_payment','paid') NOT NULL,
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ProjectHasHours`
--

CREATE TABLE `ProjectHasHours` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `uid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `iss` datetime DEFAULT CURRENT_TIMESTAMP,
  `mins` int(11) NOT NULL COMMENT 'minutes worked',
  `wage` int(11) NOT NULL COMMENT 'cents per minute',
  `status` enum('waiting','awaiting_payment','paid') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ProjectHasWage`
--

CREATE TABLE `ProjectHasWage` (
  `pid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `wage` double NOT NULL COMMENT 'euro pro stunde'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ProjectHasWage`
--

INSERT INTO `ProjectHasWage` (`pid`, `uid`, `wage`) VALUES
(4, 29, 35);

-- --------------------------------------------------------

--
-- Table structure for table `Projects`
--

CREATE TABLE `Projects` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'created by userid',
  `cid` int(11) NOT NULL COMMENT 'customer id',
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('imagefilm','website','other') NOT NULL,
  `status` enum('draft','archive','active','trash') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Projects`
--

INSERT INTO `Projects` (`id`, `title`, `slug`, `uid`, `cid`, `iss`, `type`, `status`) VALUES
(1, '\r\n', '', 0, 0, '2019-11-07 18:02:34', '', 'trash'),
(2, '12fdsa3', 'FJALKD', 6, 0, '0000-00-00 00:00:00', 'website', 'trash'),
(3, 'Testmax', 'TESTMA', 6, 5, '2019-11-08 12:36:54', 'website', 'trash'),
(4, 'third project #3', 'THIRDP', 6, 4, '2019-11-08 19:04:51', 'website', 'draft'),
(5, '323232323', '332323', 6, 0, '2019-11-08 19:06:08', 'website', 'trash'),
(6, 'test3', 'TEST41', 6, 0, '2019-11-08 19:18:19', 'website', 'trash');

-- --------------------------------------------------------

--
-- Table structure for table `QuoteHasFixed`
--

CREATE TABLE `QuoteHasFixed` (
  `id` int(11) NOT NULL,
  `cents` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Quotes`
--

CREATE TABLE `Quotes` (
  `id` int(11) NOT NULL,
  `status` enum('draft','denied','accepted') NOT NULL,
  `title` text NOT NULL,
  `uid` int(11) NOT NULL,
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `pass` text NOT NULL,
  `email` text NOT NULL,
  `policies` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`id`, `fname`, `lname`, `pass`, `email`, `policies`) VALUES
(6, 'Dominik54', 'Ziegenhagel1', '111', 'info@ziegenhagel.com', '{\"users\": [\"create\", \"list\", \"edit\", \"delete\"], \"wages\": [\"list\", \"delete\", \"create\", \"edit\"], \"quotes\": [\"list\", \"edit\", \"create\", \"delete\"], \"policies\": [\"edit\"], \"projects\": [\"listall\", \"create\", \"list\", \"edit\", \"delete\"], \"customers\": [\"create\", \"list\", \"edit\", \"delete\"]}'),
(29, 'djsa', 'g43j403', '1', 'f34232', '{\"users\": [\"list\"]}'),
(30, 'fdsaf', 'fdsa', 'asfdsa', 'fewqfew@fdsa', 'null');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Customers`
--
ALTER TABLE `Customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Files`
--
ALTER TABLE `Files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `IncoiveHasFixed`
--
ALTER TABLE `IncoiveHasFixed`
  ADD PRIMARY KEY (`invoice_id`,`fixed_id`);

--
-- Indexes for table `InvoiceHasHours`
--
ALTER TABLE `InvoiceHasHours`
  ADD PRIMARY KEY (`invoice_id`,`hours_id`);

--
-- Indexes for table `Invoices`
--
ALTER TABLE `Invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Logins`
--
ALTER TABLE `Logins`
  ADD PRIMARY KEY (`hash`);

--
-- Indexes for table `Policies`
--
ALTER TABLE `Policies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ProjectHasFixed`
--
ALTER TABLE `ProjectHasFixed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ProjectHasWage`
--
ALTER TABLE `ProjectHasWage`
  ADD PRIMARY KEY (`pid`,`uid`);

--
-- Indexes for table `Projects`
--
ALTER TABLE `Projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `QuoteHasFixed`
--
ALTER TABLE `QuoteHasFixed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Quotes`
--
ALTER TABLE `Quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Customers`
--
ALTER TABLE `Customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `Files`
--
ALTER TABLE `Files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Policies`
--
ALTER TABLE `Policies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ProjectHasFixed`
--
ALTER TABLE `ProjectHasFixed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Projects`
--
ALTER TABLE `Projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `QuoteHasFixed`
--
ALTER TABLE `QuoteHasFixed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Quotes`
--
ALTER TABLE `Quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
