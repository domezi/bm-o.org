
class Api {

    auth(body) {
        return this.call("auth", "POST", body)
    }

    getExpenses() {
        return this.call("expenses", "GET")
    }
    insertExpense({ fname, lname, email, pass }) {
        return this.call("expense", "POST", { fname, lname, email, pass })
    }
    updateExpense({ id, fname, lname, email, pass, policies }) {
        return this.call("expense/" + id, "PUT", { fname, lname, email, pass, policies })
    }
    deleteExpense(id) {
        return this.call("expense/" + id, "DELETE")
    }



    get2FA() {
        return this.call("2fa", "GET")
    }


    getInitial() {
        return this.call("initial", "GET")
    }


    getUsers() {
        return this.call("users", "GET")
    }
    insertUser({ fname, lname, email, pass }) {
        return this.call("user", "POST", { fname, lname, email, pass })
    }
    updateUser({ id, recent_pid, fname, lname, email, pass, policies, slack_id }) {
        return this.call("user/" + id, "PUT", { recent_pid, fname, lname, email, pass, policies , slack_id})
    }
    deleteUser(id) {
        return this.call("user/" + id, "DELETE")
    }


    getProjects() {
        return this.call("projects", "GET")
    }
    getProject(id) {
        return this.call("project/"+id, "GET")
    }
    insertProject({ title, slug, cid, uid, type, status }) {
        return this.call("project", "POST", { title, slug, cid, uid, type, status })
    }
    updateProject({ id, title, slug, cid, uid, type, status }) {
        return this.call("project/" + id, "PUT", { title, slug, cid, uid, type, status })
    }
    deleteProject(id) {
        return this.call("project/" + id, "DELETE")
    }


    getCustomers() {
        return this.call("customers", "GET")
    }
    insertCustomer({ title, sex, fname, lname, phone, email, address1, address2 }) {
        return this.call("customer", "POST", { title, sex, fname, lname, phone, email, address1, address2 })
    }
    updateCustomer({ id, title, sex, fname, lname, phone, email, address1, address2 }) {
        return this.call("customer/" + id, "PUT", { title, sex, fname, lname, phone, email, address1, address2 })
    }
    deleteCustomer(id) {
        return this.call("customer/" + id, "DELETE")
    }


    getWages(pid) {
        return this.call("wages/" + pid, "GET")
    }
    deleteWage({ pid, uid }) {
        return this.call("wage/" + pid + "/" + uid, "DELETE")
    }
    insertWage({ pid, uid, wage }) {
        return this.call("wage/" + pid + "/" + uid, "POST", { wage })
    }

    deleteHour(id) {
        return this.call("project/0/hour/" + id, "DELETE")
    }
    updateHour(body) {
        body.ttl = undefined;
        body.status = undefined;
        return this.call("project/0/hour/" + body.id, "PUT", body)
    }
    insertHour(pid, body) {
        return this.call("project/" + pid + "/hour", "POST", body)
    }
    getHours(pid) {
        return this.call("project/" + pid + "/hours", "GET")
    }

    deleteTask(id) {
        return this.call("project/0/task/" + id, "DELETE")
    }
    updateTask(id, body) {
        return this.call("project/0/task/" + id, "PUT", body)
    }
    insertTask(pid, body) {
        return this.call("project/" + pid + "/task", "POST", body)
    }


    getQuotes(pid) {
        return this.call("project/" + pid + "/quotes", "GET")
    }
    acceptQuote(qid) {
        return this.call("project/0/quote/" + qid + "/accept", "POST")
    }
    copyQuote(qid) {
        return this.call("project/0/quote/" + qid + "/copy", "POST")
    }
    updateQuote(body) {
        return this.call("project/0/quote/" + body.id, "PUT", body)
    }
    deleteQuote(id) {
        return this.call("project/0/quote/" + id, "DELETE")
    }
    insertQuote(pid) {
        return this.call("project/" + pid + "/quote", "POST", {})
    }
    deleteQuoteHasFixed(id) {
        return this.call("project/0/quote/0/fixed/" + id, "DELETE")
    }
    updateQuoteHasFixed(qid, body) {
        if (body.id === undefined)
            return this.call("project/0/quote/" + qid + "/fixed", "POST", body)
        else
            return this.call("project/0/quote/0/fixed/" + body.id, "PUT", body)
    }


    deleteInvoice(id) {
        return this.call("project/0/invoice/" + id, "DELETE")
    }
    updateInvoice(id, body) {
        return this.call("project/0/invoice/" + id + "", "PUT", body)
    }
    insertInvoice(pid, body) {
        return this.call("project/" + pid + "/invoice", "POST", body)
    }
    getInvoices(pid) {
        return this.call("project/" + pid + "/invoices", "GET")
    }

    call(resource, method, body) {
        this.setLoaderEnabled(true)
        return fetch("https://api.bm-o.org/" + resource, {
            method,
            headers: {
                "Authorization": "Bearer " + this.accessToken,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        }).finally(() => {
            setTimeout(()=>{this.setLoaderEnabled(false)},30)
        })
    }

    getUser() {
        return this.call("user", "GET")
    }

    getUserfield(key) {
        return (this.user !== undefined && this.user[key] !== undefined) ? this.user[key] : null
    }

    constructor({ accessToken, setAccessToken }, cb_when_ready, cb_when_error, setLoaderEnabled) {
        this.setLoaderEnabled = setLoaderEnabled
        this.accessToken = accessToken
        this.setAccessToken = setAccessToken
        this.getUser()
            .then(d => d.json())
            .catch(e => {
                cb_when_error()
                console.error("LOGIN JSON",e)
            })
            .then(r => {
                this.user = r.data
                cb_when_ready(this)
            })
            .catch(e => {
                console.error("LOGIN ER",e)
            })
            .finally(() => {
                //console.log(this.hasPolicy("users_list") ? "granted" : "denied")
            })
    }

    load(state) {
        this.user = state.user
        if(this.accessToken === undefined)
            this.accessToken = state.accessToken
    }

    setLoaderEnabled = (bool) => { }
    user = undefined
    accessToken = undefined
    setAccessToken = undefined

    hasPolicy(policy_tag) {
        // bypass policy system
        return true

        if (policy_tag === undefined)
            return true;

        // example tag: user_list
        let t = policy_tag.split("_");
        let policies
        try {
            policies = JSON.parse(this.user.policies);
            if(policies === null)
                return false;
        } catch (e) {
            return false;
        }
            
        // check for $policies["*"]=="*"
        if (policies["*"] === "*") return true;

        if (Object.keys(policies).includes(t[0])) {

            // check for $policies["user"]=="*"
            if (policies[t[0]] === "*")
                return true;

            // check for $policies["user"][i]=="list"
            if (policies[t[0]].includes(t[1]))
                return true;
        }

        return false;
    }

    getTrelloCards(id) {
        this.setLoaderEnabled(true)
        return fetch("https://api.trello.com/1/boards/"+id+"/cards?key=a30f3ba724845cad3ff5063a0d2efa39&token=ac156155369c0e9097a618d870569d6ccac4357595328582356e4354157d32be", {
            method:"GET",
            headers: {
                'Content-Type': 'application/json'
            },
            //body: JSON.stringify(body)
        }).finally(() => {
            setTimeout(()=>{this.setLoaderEnabled(false)},0)
        })
    }


    getTrelloLists(id) {
        this.setLoaderEnabled(true)
        return fetch("https://api.trello.com/1/boards/"+id+"/lists?key=a30f3ba724845cad3ff5063a0d2efa39&token=ac156155369c0e9097a618d870569d6ccac4357595328582356e4354157d32be", {
            method:"GET",
            headers: {
                'Content-Type': 'application/json'
            },
            //body: JSON.stringify(body)
        }).finally(() => {
            setTimeout(()=>{this.setLoaderEnabled(false)},0)
        })
    }

    getTrelloBoards() {
        this.setLoaderEnabled(true)
        return fetch("https://api.trello.com/1/members/me/boards?key=a30f3ba724845cad3ff5063a0d2efa39&token=ac156155369c0e9097a618d870569d6ccac4357595328582356e4354157d32be", {
            method:"GET",
            headers: {
                'Content-Type': 'application/json'
            },
            //body: JSON.stringify(body)
        }).finally(() => {
            setTimeout(()=>{this.setLoaderEnabled(false)},0)
        })
    }

}

export const projectStates = [
    { label: 'Entwurf', value: 'draft' },
    { label: 'Archiv', value: 'archive' },
    { label: 'Aktiv', value: 'active' },
    { label: 'Gelöscht', value: 'trash' },
];

export const projectTypes = [
    { label: 'Imagefilm', value: 'imagefilm' },
    { label: 'Webseite', value: 'website' },
    { label: 'Anderer', value: 'other' },
];

export const customerSex = [
    { label: 'Herr', value: 'm' },
    { label: 'Frau', value: 'w' }
];

export default Api
