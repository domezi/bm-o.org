<?php
function done($statusCode)
{
	header("Content-Type: text/html");
	//header("HTTP/1.1 " . $statusCode ." Foo bar");
	if ($statusCode == 200)
		header('HTTP/1.1 200 OK');
	else if ($statusCode == 420)
		header('HTTP/1.1 420 Parameters missing/wrong');
	else if (false && $statusCode == 403) // TODO fix
		header('HTTP/1.1 403 Forbidden');
	else if ($statusCode == 500)
		header('HTTP/1.1 500 Serverside Error');

	die($statusCode);
}

function require_policy($policy_tag)
{
	global $db;
	if (!has_policy($policy_tag)) {
		done(403);
	}
}

function has_policy($policy_tag)
{

	// example tag: user_list
	$t = explode("_", $policy_tag);

	global $db;
	$res = $db->query("select policies from Users where id = '" . UID . "'");
	if ($res->num_rows > 0) {
		$policies = json_decode($res->fetch_object()->policies, 1);
	} else return false;

	// check for $policies["*"]=="*"
	if ($policies["*"] == "*") return true;

	if (!in_array($t[0], $policies)) {

		// check for $policies["user"]=="*"
		if ($policies[$t[0]] == "*")
			return true;

		// check for $policies["user"][i]=="list"
		if (in_array($t[1], $policies[$t[0]]))
			return true;
	}

	return false;
}


function hash_pass($pass)
{
	return hash("sha512", SALT . $pass);
}

function get_update_phrase($fields)
{
	$out = "set ";
	foreach ($fields as $key => $val) {
		if ($val == "" || $key === "sendmail") continue;
		if ($out != "set ") $out .= " , ";
		$out .= " `" . str_replace("'", "", $key) . "` = '" . str_replace("'", "", $val) . "' ";
	}
	return $out;
}

function require_success($sql)
{
	global $db;
	if (!$db->query($sql))
		done(500);
}

function error_if_res($sql)
{
	global $db, $success, $data;
	$res = $db->query($sql);
	if ($res->num_rows) done(420);
}

function return_query_success($sql)
{
	global $db, $success, $data;
	$success = $db->query($sql);
}

function return_query_result($sql)
{
	return _return_query_results($sql, true);
}

function return_query_results($sql)
{
	return _return_query_results($sql, false);
}

function _return_query_results($sql, $limit1)
{
	global $db, $success, $data;

	$data = [];
	$success = $db->query($sql);
	if ($success->num_rows > 0) {
		while ($row = $success->fetch_object()) {
			$data[] = $row;
		}
	}
	if ($limit1) $data = $data[0];

	$success = boolval($success);
}

function sendmail($customer_id, $subject, $body, $uid) {
	global $db;

	$res = $db->query("select * from Users where id = '".$uid."'");
	$user = ($row = $res->fetch_assoc()) ? $row["fname"]." ".$row["lname"] : "Covomedia";

	$res = $db->query("select * from Customers where id = '".$customer_id."'");
	if($res->num_rows === 0) return false;
	$customer = $res->fetch_object(); 

	if($customer->sex === "m")
		$body = "Sehr geehrte Frau ".$customer->lname.",  <br/><br/>".$body;
	else
		$body = "Sehr geehrter Herr ".$customer->lname.", <br/><br/>".$body;

	$empfaenger = $customer->email; //Mailadresse
	$absender   = $user." <info@covomedia.com>";
	 
	$header  = "MIME-Version: 1.0\r\n";
	$header .= "Content-type: text/html; charset=utf-8\r\n";
	$header .= "From: $absender\r\n";

	$append = '<div id="footer">
			<div style="opacity:.8" id="footer-adr">
			  <div style="float:left;margin-left:5px;margin-right:20px"> <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCACAAIADAREAAhEBAxEB/8QAHQAAAgMBAQEBAQAAAAAAAAAAAAgEBgcFCQMCAf/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAWpAAKGLsZgacbcZYcAYk0EAAAIwtYpRto4RYynChlgJZthppJACOIaRDaRnAKmQC9gAlpTR9yaAvImJvA7gFNELGwN2AW8TsBiBzCkiClsPoegBhAm5yh6DbCCeaJzi1HWHrFmFnO8aEcgzUAPSYtxngnRwipH5GMIJmpVSKAAaKeh5WhDygAAF1K+coAAAH5NHPPIpgAAEolHLAAAa0acTsW8AAAJZcyggB2RtBhzmHmSRAA7x0yUaKX8X8jkg+w1RspjQiAF0GyMmMTK8MYMgecx+ALkejp9RdhNBjxvCSKKLsX0ccv4nQuB9x/DUQF+OEM8ACqiqjKjfgRRHBhDaQA5x0QAWIrZno+5MAD+GHmMnyOQO4dcqon5kA6htBNAAAChC7GinXMSM8NPGKNBAAP/EACUQAAICAgICAwACAwAAAAAAAAQGAwUBBwACECATFBUSFhEXMf/aAAgBAQABBQL0YXeoWcXG6TiOSNTIwEVWpbq1xT6ypajGwdhzVJ1PuixG4uPdQze5JMQcB21JLWy7ds9uybq8xh60q7Xrw/G2wNr6QTU7Gb3h0nbds50ifxUBZ6fqMTCZD5IIjEgfHudrLRB/nk19rDA/XwzMgqtVpF5KxrfnY2xO9zKjvBCkaGZDYC+Np9yLui5pmbEbZ4Z2oFUBZmYtpstNds5UvG3W7IAvjUzjmtP45MGFpep2E+jtCwAmrmtZPpvXG7awNJyztS7kvmoB8wp3DjI64K3s5rmzhgkIk6KFvnqMkFS8XypS6jddxmU/g1HYTxjsAoUrJsG3ZfRSrM061x8AItFwiRdpMkNdnN0znPbPNL3H1rhw2BPm9y42+IySpjJfRBoP7CzcYGANarmt0sGwn0SzvzWu1l+e09tbqWViltLIenr2tpKa7T1Fl+Aq1i+Cz9dY697RSc3Ez5LsPYWL5yn0H85w81VObdlJGswqgnlof0qq0sqQ4ryBRHWUf5FeNjsQtjyqx69bsOz0jvb2PZOm6c/ACgz0mXK/hb5aSwapp81irzbR3003wuKFm0TLmp6mnxtMn6LIVVf4G5pen+xcOa9hmX+3XMfbwor0jPexR9YY+bs75/A4ias7WkYw0QcHN4C46HgHz1pBlFDeVKMu/wBZXebbTvol8hhkJmQE7qp1XjdUP81nWuucD487z8acXCZLDwSNEaO2a1sKi4QtdQrHXydXj2UXnYO0MD9d3F/ztUdHIbTQw4a8Xz/3jSO3VHb/AHPe9eSbmve/Ud7bL8ypB7Vtby/Z61aHcdnHMeBBu5pV9r+Vsbgw4K8X2YkipZsXGljx8oabGliXG16qsxbbWvrjkCqwXk9PpexK4uItQs+v/8QAFBEBAAAAAAAAAAAAAAAAAAAAgP/aAAgBAwEBPwEAf//EABQRAQAAAAAAAAAAAAAAAAAAAID/2gAIAQIBAT8BAH//xABIEAABAwAECAoGBggHAAAAAAABAgMEAAUREhATISIxQVFhFCAjMkJxgaGx0VJicpHB4SRDgpLC8BUzRFNUY6KyBjSDhNLi8f/aAAgBAQAGPwLiFMuTef8A4drOc+XbQpq2I3ER+8dz1+Q76BkT5kh1zJiY5IvfZTS/Ws3gaD0CrGr8u+gySJKx0nXiLOxNgoiqqjdTHEY8s6gA53odmvf1UCawjNTUemjk1+XhQIjSMXJP7M/mr7NujVx1vPuJaaQLylqNgAo/FqpGLiNx5DhfUM9ZS0oizYLR/wCUJJtJ0k0RKmlUGrzYpJs5R0ersG/xpioEVDA6SukrrOvA8qro7sie5ybIaReuk9I9W+iscy1F1333gbfu205WbDQPUKlfAUyVjGt9lVAxWr8SsYo0Oh1WOT7053b8qJeYdQ+yrmuNqvJPbxHHnlhtpsXlLVoAoWmipqrGzybXp+sqle6byKpkKSBrNgHxo3WVctXnjnNRFjmb1792rwwLmSTbqbaGlxWykac/dx6ysLCNAzj8LOI5VtXO3avTkW4k/rv+vjQJUVO1c4eVY2esnf40akx3A6w6m8hY1jC4mq5KJDMJ36ewyq1Y2W7hl/IwOpP1kVaR95J+GHhEtVq1ZGmU85w/nXRUqUqwaG2hzW07KObpS/BOEVNFXY9ITa+oHmo9Ht8OvCKnlLPBZKuRJ+rc2dR8es4JU3JjrLjIOtZ0efZQT4r6hItz72UObQrbRUqqEJi1gc52q/SO1nb7OnZSrw7a1nLbIO24oWe/AqPV1ysJui0HkkdZ19Q99FSpr65D6ukrwGzAhZ+ufWsf2/DA/KeN1plBcUdwpJnPnlX1lR3bqBtptTqzoSgWml5cJUZO2UpLP95FLVVjVUc+vOR8LaRzIeYfkpTddXHcvpKttvfSFViTmtIxy/aOQdw/qwJeRGW2zbkkO8m3b7arBRmTWq01vWEdaXGVx8iwUkZHHekMmxXWKKbde4PFP7OxmpPXt4lXRFC6tDIvjYo5T3nA9FZeZiocUMc++u6ltsZSe4DtoURmnK+kj66Ra0wDuSMp7TQtNv8AAo5+ohJDKPcnT20tJtOCVVyjmSW76MvST8rfdSewiBVriWXS0l52MFrsSbNJoUNS+Cg6eCNIYPvQBQuyHVvunStxV48WKwoWsNHHPeyNXbkHbgXMmLuoGRKBzlnYKEvrxUQKtbioOanzO/i1W/bYMelBO5Wae40mOab7y1d/HvPpsnyc931RqT+dtH5kpdxhlN5RoqU/mNjIyyDkbT57+My5ouLCqTG/QeWnL18ZquazauqTnRmFj+s/DAip2FcjHz3rOkvUOweO7jst2W31hNlK1a2vFwfazvjxBHgxlyXdiNXWdVMfWDzM6smbDiEG1DNugkbev54JUxzmMNqcI22Cj0h43nXVlaztJ4inWI6iwnnPrIQ0OtaslDwuuGyv93BaLx95up9xNE4uFWUxAGXHSUNW9gQfGkGIx/hbEurctDhnuKu2ZbbNeijNYsy4cW1GKWJTuLvEaLPzqpnVnVI/3yKfSa+hDdHS46f7bO+gIjza3c/nqEdv3JtV304PELVUxf3NXoxXfp76IecFj0xZfNumzQnz7cDyBpkOoa/F+HDdhMckOc+5kbT2/AUQ5MH6TkjW6OTH2fOgjSWxLgOMpdQ1bdUz0TcVq5ujKMuiipcRfCoY5yrLFte2NXXowSqxUMyM3cRk6Svlb76SIdnLfrGTsWNHl20KVApUMhB1YY8JNuLtvvKHRQNP530S2hIShIupSNQwQU6jJt/pOBE+uAtqKbC3GGRTg2nYO/q1oZYbSyygWJQgWAYKrk63G1t/dIP4qB6Ou4vQdYUNYI1jdRdb1M0UYn/OwAbS166Ndzw6tEeKoWSFcq/7Z8sg7MH6aiIPB31fSEpGRC/S7fHrwIaaQpx1ZupQkWkmnKZZ8ixT6rdGxI6sMVz0JQ70qo1W9atctzo8dXQ9ZW/Zs8MNSf634MC65UpxmK2ktoANgeJ09YHjZswuMPoDjLibqkHWKIagMOTYklVjCkC0p9VWzroJku6/WZHO6LO5O/fxEtyWkvISsOBKto0cRyramdtdOa7MQeZuRv3/AJFWxtTbJc+8bPw0ClBTVXNnlX9vqp3+FGo0dsNMNJuoQnUOKqVU881hF1xHGUFxHVkzvHrpYY8G0bW1f8qEBqE2fSS2rJ71UbhR56y6+q6lDTaU94FtlI8Zb7kpxtFi3nVEqWrWctuDGzpAbJGa0Mq19QoqNFBg1ebQUA57g9Y/Ad9GY7QvOOrCEjaTQzZ7mKqxhCWm20HPdGnsFqjvyUbjxmkssNixKE6BxyqVHuyP4hnNc+fbQqq6U1LR6DuYvyPdRVZ1qPp6xZYkFeJT9nXv+dFBpmVKWP5RbT2lXlQoq9ngLRycim+v73kBRToq+W+4s2l14EXvtKoF1jJagp9BHKL8vGgXGj4ySP2l/OX2bNOri//EACcQAQABBAAGAgIDAQAAAAAAAAERACExQRBRYXGBkSChscHR8PHh/9oACAEBAAE/IfgQASS/dzHkKevmBZuSFuxS2AhzS0j6opmNUvKQf2dKQhhJ4k+wGvSCEyCJHkwtKYf1A+cWJXYKdjqG/CYtTcwZj5ocfCi2tIwvcy+OVN7tuYpEzpRKtCtH0AHIb9IBU2wIEnv98nONcLjpKdiEAS6SBun+V3RfLPemlrWUvuoYytJCpG5S2ziSBPMN8sFBdzIWjYWfg/fqkCytck+UUfdyNd5UGX7MSB9qLOAs7kL+7N7cUoLmY1B+3RUIEYoUIDiLBLYpiS7eXu/9ZcqzwH0/pH6EOkPljEp/ccUHcwB0m0Scyd4omFYVpyvbjnbODnLkFpViTaDHkZX7B+3dAyyEOhN++N6xmh0/bPRjiaCvIQn/AA+FrnAMMFlaVm4ujkqkSgppVkOj7m5CDUYzMuDY2jnVe4ip+pCqIwI8DgBEF0yzozx5JCRWZyj4ORgXwW4HwxJuRH5PhBLeiYCX8VOyApkGh0CA6FZPVq3YK5Pwlva4omLoQez50Aaw8PkAM2hFpphU4xsrC9RKBLBmkaWEA+4H3Uz3810MI7zjFQY9tfB1582Oh8GMrV6D9nAg4s5iY94H4VlzGwTaw+wVcJi4R1E+xpwpMq3eGtND4QdWvGmzD+eQVMjqhuPlIPJaKy+ZJe63+LZxyJJy7u4Dmgwume38bpV3gka9LbmxBb4uMCZYGl9tXpbgcst+Wagaijv9XKvVZgoUnOD0DmrAG1KZW7mnKdVl2eRAfG9ORGoZqwqWDKzPk8JgEnFh10ZknlwwreVZT7L2/m++QksVhWIliCH4USW8VhzSw6qUwUqFbcxdgSIDkMHBdhnpFwx3cV1KvapX2/CxzBJeSAtyy1bsIIPdinmnUhXgAHHPSpahBrHOUiAmGh/J3QSpRCwx4VO2bJOngabef+JioYMyMp5MjetO5zm1LzTe89qa+kN36EOC1nSmesvpHnjkQY967NzI9KyVUwm6YvtztWa0VjeKOvIlkpiJR3gcysBfu0FWBmQp8xifKHofTR+ESXb+Uuuip74ocK2JxA8PmJE++jqKH08CACAOBJtqjqR/l4ShEZspcviMs6Uj7wOfoHCExJGWeQVV6AgkrlrJhVmkmkRJvuK9m8XN1Dz2zokmdCxm7fBXsSBl4s1v9rTgY5cqJgA203LJchGeyk32rqI4QhKjeUftBUpBQTYaL2Hc3j5LxwkZGCdE/wAzi53WNYhKux4gGeyNrITpCx48FwS/0HxLTPHDvZGeV7+ASp1gDaf46vejXfkeKXFAfR+0/pd0JUsYgPgkE58qI55QIjYCc+lrmpbhhBtW7DEl7BRnFq4vOiGVnAzRPBZIpVK7LE2xg4Sp0fFWfpLAbSv4FmNoI6V2WykojflEB7aDVYIETuarLsxI1GsX8B8xxsQWju4w0auT2lqNE37k9lLJGRbvCDdu61jmxceiY5QU9qBNWmV5IiRnqir+rHU7YBfNTUmF8wMIO407LUNuURaiwkzPx//aAAwDAQACAAMAAAAQkkkgAgkkkgkEAkgkgAEgkgEkkkkgEgkEgEkkEgEAAAkkkAAgEkkgkkkgEkkkEkkkkkkkEkkgEkAgkkAEAgkgkEAkAkEggEAAkgEgkkkkkgkkkEgEkkkEgAkkkkAggkkk/8QAFBEBAAAAAAAAAAAAAAAAAAAAgP/aAAgBAwEBPxAAf//EABQRAQAAAAAAAAAAAAAAAAAAAID/2gAIAQIBAT8QAH//xAAiEAEBAQACAQUBAQEBAAAAAAABESEAMUEQIFFhcYGRocH/2gAIAQEAAT8Q9kFVVMAgKVERK+F5boZBPjFTVAvnNueiIQKA5AV+B9cChtfxMKRPNibxqiJsJGiTfI++QkyB0iOqRUivI4dl4B5+kXOv0tzuQ2SUH2Nx6o6T3h8sMIqjA5oP8Mm0zDhlfZTfZgk1VdVfPG2fE0o3FRQUa1FJeew/VGqTAUqAM9ILVWo0yB1xaXL9IVXbe/zlB+rwT1pmPwgg+68ShBgRf2M/zhb9YL6FJLwNsuBCwgMCpagImPY+wImeyaqegDn3d1UCHl7XDAK4CMCoVoYiuZEaH43tyEN1E1ERYOr6xpAsiqdvo8wQFjgqGlpM7OwPZXdVr6gyAFV6OO2y+DwA8GgGdlIA/wDUqNgioAClAEE52fkmxKMYj4UCIiCJ6xHtuwlrqhfQs29KfbCIFP0hZ5T1Jpe+E4UPyvCBvCbYuq1UHyuKlXwAEgwOfBv9T/fVz6borKB2yV7BQU9AduEnVBYiyChcHoJodVW3UkZTWfjrne4JDt1rEMILTUZG1uBN5WQQ4SIIWRGUVyTGWTnXLBvUqpq8mKB0pGNobeygYFUMFYHoKYw9FMsM+153wceIpwl5hqxQNXOIosoSY52P6RxkBRo/gSv8OD06QEtjisdKZwadxc/ATUnzPheGE3pyAEE0l0lAUx4iFQB2KT64QgVMA7eUGHgA9LBYadnnkgbmZ0ZmKKWcBvYNfQSzbKNZT2RLBIiP/EX+elj44iO0t8IaqUjsxBWgSsaiKQgjpu5Cllp5j7beV47x6sp8q+jR4GWLr0KkTfkmK3dK4VAFBJknyjU6z7O5t0LGFs5JrmLfVav6+03IAWVEPIq/SfHp48cCKqKUnmAqgF48JDWMRsIppdxNHsWaI8rB+J5V2Teac/e+4FAFXoOX3G5RDt8mB4tCcQK10roGpW8IHniNqFqeD6BEPQB7tqPM1Y6D+c/FGAzn7z3RwUYGFyqFqAAzH6J1qwNOmaUYxIlHv2dnTGOL4t4kIFQgMD6Cf57BFHQdUuF+RKm7zM9BIZAegAqo9AAQgRYk18gDHUx5mNJZTm8VH2NgdCCnwTgK0g8RsxPJXch8HqZF3lorKFiAtIui0tvby1A0OhKMjjwpAcihq1MhRCVLGQig/wBMF/5w2SKrfPBrdSn9Jw5EnT2gmhjNdvEH3NRIqKDyjyMIVgDOkAvbD99vn0eSBmELh81Q+F6vntOLMpFhSEW4rwgdQlASXRjN2kCgWyv1RshqmqQIuHmY+JhrqQc6Q/Ts/TJo3HpUJDZ0Xd/nqA62vQVXhXc49B5JrENEREfW5EQR3DGKliSuPIylRMAeAAA+vQvInTENfw/29FVr0x0LAwIK0BceXfrwYIDVf1fQjx+MgS9A3E1rejg9noBIKR8VBRE5V6yyxSqjibNqTwAoFIj5hEOhU+09G0CAUQTEe0mWqQcdiHAeHaigB3eG2HYHDAzshatx9SD5KSCVf4T9eZnt4wVk3qn6GXuHMz5inl0fQCEKmehWEdrZH6PjeKqCjQqbpEthqzi20cBg13aFwMWSfshDPZSV4OwGNEoiKeqgVw4EdqUzBXXojPnngnGHD2XTzc251PPDf9CoyKqIBKxE0XlFGgSGQN1fKqqqqqvsNSgI0j/p1xUpby/RUQAJs8VtopuJjSI8GwMTfcH+gPGZXR10kFFOGKC86d4NNM24NhQYHop14dwYDQp2kmi8E9FAcwn2jU6YcTENTDmqFQO+EbzG9TSXlFQADANCbvcPKqquqqqq++8thGQCIiAAKFk5f6izYz0pigXYsBttEMmIs6UAoCuT1YG7/AfuP/TjanImocZqghSODxkEhYrU0rVU7XhEAh/fUOYaXhmngCaw1D0Nh6oYX2//2Q==" alt="Logo" style="border-radius:50%" class="transparent" height="58" width="58"><br>
			  </div>
			  <div style="float:left;"><b>Covomedia GbR</b><br>
				Bayreutherstrasse 21<br>
				01187 Dresden<br>
				<a style="cursor:pointer" href="https://www.covomedia.com/">www.covomedia.com</a><br>
				<a style="cursor:pointer" href="mailto:info@covomedia.com">info@covomedia.com</a><br>
				<a style="cursor:pointer" href="tel:+4915258799871">+49 1525 87 99 87 1</a><br>
			  </div>
			  <br style="clear:both">
		  </div>
		</div>
		<div style="padding:12px 14px;font-size:.8em;font-style:italic;margin-bottom:0px;margin-top:-25px;margin-left:-20px;margin-right:-20px;opacity:.8;background:hsla(0,0%,0%,.1);border-top:hsla(0,0%,0%,.1);">Diese Email enthält vertrauliche und/oder rechtlich geschützte Informationen. Wenn Sie nicht der richtige Adressat sind oder diese E-mail irrtümlich erhalten haben, informieren Sie bitte sofort den Absender und vernichten Sie diese Mail. Das unerlaubte Kopieren sowie die unbefugte Weitergabe dieser Mail ist nicht gestattet.
	</div><style>
	body {
		background:#fff;
		color:#333;
		padding: 20px 10px;
		padding-top:8px;
	}

	body * {
		font-size:1.03rem;
		font-family: arial,sans !important;
	}
	#logo {
		position:fixed;
		right:10px;
		top:10px;
	}
	a {
		color:#688200 !important;
		text-decoration:none !important;
	}
	a:hover, a:focus {
		color: #9ECA1A !important;
	}
	a.btn {
		display: inline-block;
		transition: .1s;
		padding: 10px 15px;
		border-radius: 3px;
		border:1px solid #888 !important;
		color: #333 !important;
		cursor:pointer
		box-shadow: 0px 2px 5px #0002;
		background-image: linear-gradient(#0000,#0002);
	}
	a.btn:hover {
		cursor:pointer;
		background: rgba(0,0,0,.07);
		background-image: linear-gradient(#0001,#0000);
		opacity:1;
	}
	#footer-adr {
		background:#f9f9f9;
		border-top:1px solid hsla(0,0%,0%,.1);
		margin-top:30px;
		padding:13px 17px;
		margin:25px -20px;
	}
	#footer {
		margin-top:2px;
	}
	</style>
	  ';
	 
	return (mail( $empfaenger,
	      $subject,
	      $body."<br/><br/>mit freundlichen Grüßen,<br/>".$user.$append,
	      $header)) ? "success sendmail" : "error sendmail";

}

function slack_msg($msg) {
	shell_exec("curl -X POST -H 'Content-type: application/json' --data '{\"text\":\"".str_replace("\"","\\\"",$msg)."\"}' https://hooks.slack.com/services/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");
}

function slack_pmsg($msg) {
	shell_exec("curl -X POST -H 'Content-type: application/json' --data '{\"text\":\"".str_replace("\"","\\\"",$msg)."\"}' https://hooks.slack.com/services/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");
}

function euros($euro) {
	return  number_format($euro, 2, ',', '');
}

function slack_action($action,$params) {
	$token = "xoxb-974401580018-987217117013-7aAawpctK2JrHRBzftqVnLUr";

	$params["token"] = $token;

	$curl = curl_init();
	if($action == "channels.create") {
		curl_setopt_array($curl, array(
					CURLOPT_URL => "https://slack.com/api/".$action,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => "token=".$token."&name=".$params["name"]."&validate=true",
					CURLOPT_HTTPHEADER => array(
						"Content-Type: application/x-www-form-urlencoded"
						),
					));
	} else if($action == "chat.postMessage") {
		$params["channel"] = strtolower($params["channel"]);
		curl_setopt_array($curl, array(
					CURLOPT_URL => "https://slack.com/api/".$action,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS =>http_build_query($params),
					CURLOPT_HTTPHEADER => array(
						"Content-Type: application/x-www-form-urlencoded"
						),
					));
	}

	$response = curl_exec($curl);

	curl_close($curl);

}



