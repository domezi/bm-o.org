<?php
//============================================================+
// License: GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// -------------------------------------------------------------------
// Copyright (C) 2016 Nils Reimers - PHP-Einfach.de
// This is free software: you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Nachfolgend erhaltet ihr basierend auf der open-source Library TCPDF (https://tcpdf.org/)
// ein einfaches Script zur Erstellung von PDF-Dokumenten, hier am Beispiel einer Rechnung.
// Das Aussehen der Rechnung ist mittels HTML definiert und wird per TCPDF in ein PDF-Dokument übersetzt. 
// Die meisten HTML Befehle funktionieren sowie einige inline-CSS Befehle. Die Unterstützung für CSS ist 
// aber noch stark eingeschränkt. TCPDF läuft ohne zusätzliche Software auf den meisten PHP-Installationen.
// Gerne könnt ihr das Script frei anpassen und auch als Basis für andere dynamisch erzeugte PDF-Dokumente nutzen.
// Im Ordner tcpdf/ befindet sich die Version 6.2.3 der Bibliothek. Unter https://tcpdf.org/ könnt ihr erfahren, ob 
// eine aktuellere Variante existiert und diese ggf. einbinden.
//
// Weitere Infos: http://www.php-einfach.de/experte/php-codebeispiele/pdf-per-php-erstellen-pdf-rechnung/ | https://github.com/PHP-Einfach/pdf-rechnung/

require("../dbcon.php");

$rechnungs_nummer = "743";
$rechnungs_datum = date("d.m.Y");
$pdfAuthor = "Covomedia";

$rechnungs_header = '
Covomedia GbR | Bayreutherstr. 21, Dresden';

$rechnungs_empfaenger = 'Max Musterman
Musterstraße 17
12345 Musterstadt';

$rechnungs_footer = "Wir bitten um eine Begleichung der Rechnung innerhalb von 28 Tagen nach Erhalt. Bitte Überweisen Sie den vollständigen Betrag an:
";
$user_bankdetails="<b>Empfänger:</b> Covomedia GbR<br><b>IBAN</b>: DE8*<br><b>BIC</b>: C4*<br>";

$rechnungs_header2 = "";

//Auflistung eurer verschiedenen Posten im Format [Produktbezeichnuns, Menge, Einzelpreis]
$rechnungs_posten = array();
$pid = 0;
$title = "Rechnung";
$user = "Covomedia GbR";
$anrede = array("m" => "Herr", "w" => "Frau", "o" => "");
$nachname_kunde = "Kunde";
$project_slug = "COVO";

$invoice_mins = 0;
$invoice_euros = 0;




if ($_GET["type"] == "quote") {
	$sql = "select Quotes.status,Quotes.iss,QuoteHasFixed.*, Quotes.pid from Quotes, QuoteHasFixed where Quotes.id = QuoteHasFixed.qid and Quotes.pid = '" . intval($_GET["pid"]) . "' and Quotes.id = '" . intval($_GET["id"]) . "'";
	$res = $db->query($sql);
	while ($row = $res->fetch_object()) {
		$rechnungs_posten[] = array($row->text, 1, $row->euros);

		if ($pid == 0) {
			$pid = $row->pid;
			$rechnungs_datum = date("d.m.Y", strtotime($row->iss));
			if ($row->status == "accepted") {
				$title = "Angebotsbestätigung";
				$rechnungs_nummer = "AB" . $_GET["id"];
				$rechnungs_header2 = "<br>Gerne bestätigen wir Ihnen das Angebot AG" . $_GET["id"] . ":<br>";
				$rechnungs_footer = "Wir freuen uns über die Auftragserteilung und sichern eine erfolgreiche Ausführung zu.<br>Wir bitten um Zahlung nach Rechnungsstellung.<br><br>";
			} else {
				$title = "Angebot";
				$rechnungs_nummer = "AG" . $_GET["id"];
				$rechnungs_header2 = "<br>Gerne bieten wir Ihnen an:<br>";
				$rechnungs_footer = "Wir freuen uns auf eine Auftragserteilung und sichern eine erfolgreiche Ausführung zu.";
			}
		}
	}
} else if ($_GET["type"] == "invoice") {
	$sql = "select * from Invoices i,InvoiceHasHours ih,Hours h where h.id = ih.hours_id and ih.invoice_id = i.id and i.pid = '" . intval($_GET["pid"]) . "' and i.id = '" . intval($_GET["id"]) . "'";
	$res = $db->query($sql);
	while ($row = $res->fetch_object()) {

		if ($row->mins > 0) {
			$invoice_euros += $row->euros;
			$invoice_mins += $row->mins;
		} else
			$rechnungs_posten[] = array($row->text, 1, $row->euros);

		if ($pid == 0) {
			$pid = $row->pid;
			$rechnungs_datum = date("d.m.Y", strtotime($row->iss));
			$due = date("d.m.Y", strtotime($row->iss) + 86400 * 21);
			$title = "Rechnung";
			$rechnungs_nummer = "R" . $row->invoice_no;
			$rechnungs_header2 = "<br>Wir bedanken uns für die gute Zusammenarbeit und senden Ihnen vereinbarungsgemäß die Rechnung für folgende Leistungen:<br>";
			$rechnungs_footer = "Wir bitten um Zahlung des Rechnungsbetrages bis zum " . $due . ".<br><br>";
		}
	}
}

if ($invoice_mins > 0 || true)
	$rechnungs_posten[] = array("Zusätzliche Arbeitszeit von ca. " . round($invoice_mins / 60) . " Stunden", 1, $invoice_euros);

if (count($rechnungs_posten) == 0) {
	die("<h1 style=margin-top:100px;text-align:center>Entschuldigung, die angefragte Datei wurde nicht gefunden.</h1><a style=display:block;text-align:center href='https://www.covomedia.com'>www.covomedia.com</a>");
}


$sql = "select bankdetails,p.slug,p.title,c.*,u.fname as ufname, u.lname as ulname from Projects p, Customers c,Users u where u.id = p.uid and p.cid = c.id and p.id = '" . $pid . "'";
$res = $db->query($sql);
if($res->num_rows) {
    while ($row = $res->fetch_object()) {
	    $project_slug = $row->slug;
	    $rechnungs_empfaenger = $anrede[$row->sex] . " " . $row->title . $row->fname . " " . $row->lname . "\r\n" . $row->address1 . "\r\n" . $row->address2;
	    $user = $row->ufname . " " . $row->ulname;
	    $nachname_kunde = $row->lname;
        $tmp_user_bankdetails = json_decode($row->bankdetails);
        $user_bankdetails="<b>Empfänger:</b> ".$user."<br><b>IBAN</b>: ".$tmp_user_bankdetails->IBAN."<br><b>BIC</b>: ".$tmp_user_bankdetails->BIC."<br>";
    }
} else {
    die("Beteiligte Personen konnten nicht ermittelt werden.");
}

if ($_GET["type"] == "invoice") {
    $rechnungs_footer .= $user_bankdetails."<br>Vielen Dank für Ihr Vetrauen in unsere Dienstleistungen.<br><br>";
}
    

//Höhe eurer Umsatzsteuer. 0.19 für 19% Umsatzsteuer
$umsatzsteuer = 0.0;

$pdfName = $project_slug . "_" . $rechnungs_nummer . "_" . $nachname_kunde . ".pdf";


//////////////////////////// Inhalt des PDFs als HTML-Code \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


// Erstellung des HTML-Codes. Dieser HTML-Code definiert das Aussehen eures PDFs.
// tcpdf unterstützt recht viele HTML-Befehle. Die Nutzung von CSS ist allerdings
// stark eingeschränkt.

$html = '
<div style="text-align:right;margin-top:0px">
<img src="logo.png" style="width:80px;display:block">
</div>';
$html .= '
<table cellpadding="5" cellspacing="0" style="width: 100%; ">
	<tr>
		<td><u>' . nl2br(trim($rechnungs_header)) . '</u></td>
	</tr>

	<tr>
		<td colspan="2">' . nl2br(trim($rechnungs_empfaenger)) . '</td>
	</tr>
</table><div style="text-align:right">';
if ($_GET["type"] == "invoice")
	$html .= ' Rechnungsnummer ' . $rechnungs_nummer . '<br> Rechnungsdatum: ' . $rechnungs_datum;
else
	$html .= ' Angebotsnummer ' . $rechnungs_nummer . '<br>Angebotsdatum: ' . $rechnungs_datum;
$html .= '</div>
<div style="font-size:1.3em;font-weight:bold;">' . $title . '</div>
' . $rechnungs_header2 . '<br>
<table cellpadding="5" cellspacing="0" style="width: 100%;margin-top:10px" border="0">
	<tr style="background-color: #cccccc; padding:5px;">
		<td style="padding:5px;"><b>Bezeichnung</b></td>
		<td style="text-align: right;"><b>Preis</b></td>
	</tr>';


$gesamtpreis = 0;

foreach ($rechnungs_posten as $posten) {
	$menge = $posten[1];
	$einzelpreis = $posten[2];
	$preis = $menge * $einzelpreis;
	$gesamtpreis += $preis;
	$html .= '<tr>
                <td>' . $posten[0] . '</td>
                <td style="text-align: right;">' . number_format($preis, 2, ',', '') . ' Euro</td>
              </tr>';
}
$html .= "</table>";



$html .= '
<hr>
<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
if ($umsatzsteuer > 0) {
	$netto = $gesamtpreis / (1 + $umsatzsteuer);
	$umsatzsteuer_betrag = $gesamtpreis - $netto;

	$html .= '
			<tr>
				<td colspan="3">Zwischensumme (Netto)</td>
				<td style="text-align: right;">' . number_format($netto, 2, ',', '') . ' Euro</td>
			</tr>
			<tr>
				<td colspan="3">Umsatzsteuer (' . intval($umsatzsteuer * 100) . '%)</td>
				<td style="text-align: right;">' . number_format($umsatzsteuer_betrag, 2, ',', '') . ' Euro</td>
			</tr>';
}

$html .= '
            <tr>
                <td colspan="3"><b>Gesamtsumme: </b></td>
                <td style="text-align: right;"><b>' . number_format($gesamtpreis, 2, ',', '') . ' Euro</b></td>
            </tr>			
        </table>
<br><br><br>';

if ($umsatzsteuer == 0) {
	$html .= 'Nach § 19 Abs. 1 UStG wird keine Umsatzsteuer berechnet.<br><br>';
}

$html .= nl2br($rechnungs_footer);

$html .= "<br>Bei Fragen stehen wir Ihnen gerne jederzeit zur Verfügung.<br><br>
Mit freundlichen Grüßen,<br>
" . $user;




//////////////////////////// Erzeugung eures PDF Dokuments \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// TCPDF Library laden
require_once('tcpdf/tcpdf.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{
	//Page header
	public function Header()
	{
		// get the current page break margin
		$bMargin = $this->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $this->AutoPageBreak;
		// disable auto-page-break
		$this->SetAutoPageBreak(false, 0);
		// set bacground image
		$img_file = 'background.jpg';
		$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// restore auto-page-break status
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$this->setPageMark();
	}

	public function Footer()
	{
		// Position at 15 mm from bottom
		//$this->SetY(-29);
		// Set font
		//$this->SetFont('helvetica', 'R', 8);
		//$this->SetTextColor(150,150,150); 

		// Page number
		//$this->Cell(0, 10, 'www.covomedia.com - info@covomedia.com', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}


// Erstellung des PDF Dokuments
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Dokumenteninformationen
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor($pdfAuthor);
$pdf->SetTitle('Rechnung ' . $rechnungs_nummer);
$pdf->SetSubject('Rechnung ' . $rechnungs_nummer);


// Header und Footer Informationen
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// Auswahl des Font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// Auswahl der MArgins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// Automatisches Autobreak der Seiten
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// Image Scale 
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// Schriftart
$pdf->SetFont('dejavusans', '', 10);

// Neue Seite
$pdf->AddPage();

// Fügt den HTML Code in das PDF Dokument ein
$pdf->writeHTML($html, true, false, true, false, '');

//Ausgabe der PDF

//Variante 1: PDF direkt an den Benutzer senden:
$pdf->Output($pdfName, 'I');

//Variante 2: PDF im Verzeichnis abspeichern:
//$pdf->Output(dirname(__FILE__).'/'.$pdfName, 'F');
//echo 'PDF herunterladen: <a href="'.$pdfName.'">'.$pdfName.'</a>';
