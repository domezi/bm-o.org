<?php
switch (METHOD) {
    case "POST":
        require_policy("customers_create");
        $sql = "INSERT INTO `Customers` (`id`, `title`, `sex`, `address1`, `address2`,  `email`,  `phone`,  `iss`, `lname`, `fname`, `uid`) 
        VALUES (NULL, '" . $body["title"] . "', '" . $body["sex"] . "','" . $body["address1"] . "','" . $body["address2"] . "',
        '" . $body["email"] . "', '" . $body["phone"] . "',CURRENT_TIMESTAMP,'" . $body["lname"] . "', '" . $body["fname"] . "','".UID."')";
        require_success($sql);
	$out["insert_id"]=$db->insert_id;
        break;
    case "DELETE":
        require_policy("customers_delete"); // kann sämtliche customers löschen
        return_query_success("delete from Customers where id = '" . ROUTE[1] . "'");
        break;
    case "PUT":
        require_policy("customers_edit");

        $sql = "update Customers " . get_update_phrase($body) . " where id = '" . ROUTE[1] . "'";
        return_query_success($sql);
        break;
    case "GET":
        return_query_result("select * from Customers where id = '" . UID . "' limit 1");
        break;
}
