<?php
require_once __DIR__.'/../GoogleAuthenticator.php';
$ga = new PHPGangsta_GoogleAuthenticator();

switch (METHOD) {
    case "POST":
        $res = $db->query("select * from Users where email like '" . str_replace("_", "\_", $body["email"]) . "' and pass = '".hash_pass($body["pass"])."'"); 
        if ($res->num_rows) {
	    $user = $res->fetch_object();
	    if($user->secret !== "") {

		if($body["code"] === "") die(json_encode(array("codeRequired"=>true)));

		$data["2fa_success"] = $ga->verifyCode($user->secret, $body["code"], 2);
		if(!$data["2fa_success"])
			done(403);

	    }
            $hash = hash("sha512", rand(0, 9e9) . uniqid());
            require_success("insert into Logins(hash,uid,server) values('" . $hash . "','" . $user->id . "','".json_encode($_SERVER)."')");
	    slack_pmsg(":white_check_mark: Erfolgreicher Login mit \"".$body["email"]."\" am ".date("d.m.Y, \u\m H:i")." Uhr.");
            $data["hash"] = $hash;
        } else {
	    slack_pmsg(":exclamation: Fehlgeschlagener Login mit \"".$body["email"]."\" am ".date("d.m.Y, \u\m H:i")." Uhr.");
            done(403);
        }
        break;
    case "GET":
        $data["info"] = "please use post";
        break;
}
