<?php
switch (METHOD) {
	case "GET":

	$success = true;

	// projects
	$out["projects"]=[];
	if(has_policy("projects_listall"))
	    $sql=("select * from Projects where status != 'trash'");
	else
	    $sql=("select distinct Projects.* from Projects, ProjectHasWage where (( Projects.id = ProjectHasWage.pid and ProjectHasWage.uid = '".UID."' ) or  Projects.uid = '".UID."' ) and Projects.status != 'trash'");

	$res=$db->query($sql);
	if ($res->num_rows) {
		while ($row = $res->fetch_assoc()) {
			$out["projects"][] = $row;
		}
	} 

	// customers
	$out["customers"]=[];
        if (has_policy("customers_list"))
            $sql=("select * from Customers");
        else
            $sql=("select * from Customers where uid = '" . UID . "'");

	$res=$db->query($sql);
	if ($res->num_rows) {
		while ($row = $res->fetch_assoc()) {
			$out["customers"][] = $row;
		}
	} 



	// users
	$out["users"]=[];
        if (has_policy("users_list"))
            $sql=("select  secret,id,fname,lname,email,'' as pass,policies,wage,bankdetails,recent_pid from Users");
        else
            $sql=("select  secret, id,fname,lname,email,'' as pass,policies,wage,bankdetails,recent_pid from Users where id = '" . UID . "'");

	$res=$db->query($sql);
	if ($res->num_rows) {
		while ($row = $res->fetch_assoc()) {
			$row["secret"] = ($row["secret"] == "" ) ? "":"*";
			$out["users"][] = $row;
		}
	} 

	$data = true;

	break;
}
