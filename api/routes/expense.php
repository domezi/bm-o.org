<?php
switch (METHOD) {
    case "POST":
        require_policy("expenses_create");

        //$sql = "INSERT INTO `Users` (policies,`id`, `fname`, `lname`, `pass`, `email`) VALUES ('null',NULL, '" . $body["fname"] . "', '" . $body["lname"] . "', '" . hash_pass($body["pass"]) . "', '" . $body["email"] . "')";
        $sql = "INSERT INTO `Expenses` (`id`, `uid`, `euros`, `status`, `iss`, `description`, `invoice_uri`, `tag`, `depreciation_end`, `depreciation_euros_monthly`, `depreciation_euros`, `product_uri`) VALUES (NULL, '".UID."', '".$body["euros"]."', 'open', CURRENT_TIMESTAMP, '".$body["description"]."', '', '".$body["tag"]."', '0000-00-00', '0', '0', '".$body["product_uri"]."')";
        return_query_success($sql);

        break;
    case "DELETE":
        if (ROUTE[1] == UID)
            done(420);
        require_policy("expenses_delete"); // kann sämtliche expense löschen
        return_query_success("delete from Expenses where id = '" . ROUTE[1] . "'");
        break;
    case "PUT":

	require_policy("expenses_edit");
        $sql = "update Expenses " . get_update_phrase($body) . " where id = '" . ROUTE[1] . "'";

        return_query_success($sql);
        break;
    case "GET":
        return_query_result("select * from Expenses where id = '" . UID . "' limit 1");
        break;
}
