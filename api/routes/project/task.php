<?php
switch (METHOD) {
    case "POST":
        require_policy("hours_create");
        $sql = "INSERT INTO `Tasks` (`id`, `pid`, `title`, `mins`, `euros`, `description`, `iss`, `deadline`, `uid`, `assignee_uid`) 
        VALUES (NULL, '".ROUTE[1]."', '".$body["title"]."', '".$body["mins"]."', '".$body["euros"]."', '".$body["description"]."', CURRENT_TIMESTAMP, '".date("Y-m-d H:i:s",strtotime($body["deadline"]))."', '".UID."', '".$body["assignee_uid"]."')";
        echo $sql;
        return_query_success($sql);
        break;
    case "DELETE":
        require_policy("hours_delete");
        $sql = "delete from Tasks where id = '" . ROUTE[3] . "'";
        return_query_success($sql);
        break;
    case "PUT":
        require_policy("hours_edit");
        $sql = "update Tasks " . get_update_phrase($body) . " where id = '" . ROUTE[3] . "'";
        return_query_success($sql);
        break;
}
