<?php
switch (METHOD) {
    case "POST":
        require_policy("hours_create");
        $sql = "INSERT INTO `Hours` (`id`, `text`, `uid`, `pid`, `iss`, `mins`, `euros`, `tid`) 
        VALUES (NULL, '" . $body["text"] . "', '" . UID . "', '" . ROUTE[1] . "', CURRENT_TIMESTAMP, '" . $body["mins"] . "', '" . $body["euros"] . "', '" . $body["tid"] . "')";
        return_query_success($sql);
        break;
    case "DELETE":
        require_policy("hours_delete");
        $sql = "delete from Hours where id = '" . ROUTE[3] . "'";
        return_query_success($sql);
        break;
    case "PUT":
	if(has_policy("hours_edit")) 
		$sql = "update Hours " . get_update_phrase($body) . " where id = '" . ROUTE[3] . "'";
	else if(has_policy("hours_create")) 
		$sql = "update Hours " . get_update_phrase($body) . " where uid = '".UID."' and id = '" . ROUTE[3] . "'";
        return_query_success($sql);
        break;
}
