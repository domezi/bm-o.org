<?php
switch (METHOD) {
    case "POST":
        require_policy("invoices_create");

        $invoice_no_max = $db->query("select max(invoice_no) as max from Invoices where invoice_no > " . date("ym") . "00");
        $max = intval($invoice_no_max->fetch_object()->max);
        if ($max > 0)
            $invoice_no_max = $max  + 1;
        else
            $invoice_no_max = date("ym") . "01";

        $sql = "INSERT INTO `Invoices` (`id`, `pid`, `status`, `iss`, `invoice_no`, `uid`) 
        VALUES (NULL, '" . ROUTE[1] . "', 'awaiting_payment', CURRENT_TIMESTAMP, '" . $invoice_no_max . "', " . UID . ")";
        return_query_success($sql);
        $iid = $db->insert_id;

        $hours_sql = "select * from Hours where pid = '" . ROUTE[1] . "'";
        if ($body["start"] != "")
            $hours_sql .= " and iss >= '" . date("d.m.Y H:i:s", strtotime($body["start"])) . "'";
        if ($body["end"] != "")
            $hours_sql .= " and iss <= '" . date("d.m.Y H:i:s", strtotime($body["end"])) . "'";

        //is already somewhere ? 
        //"select * from Hours h left join InvoiceHasHours ih on ih.hours_id = h.id where ih.invoice_id is null "

        $hours = $db->query($hours_sql);

        $euros = 0;

        while ($hour = $hours->fetch_object()) {
            if ($db->query("select * from InvoiceHasHours where hours_id= '" . $hour->id . "'")->num_rows == 0) {
                $euros +=  $hour->euros;
                if (intval($body["euros"]) <= 0 || $euros < intval($body["euros"])) {
                    //if okay, continue adding
                    $db->query("insert into InvoiceHasHours(invoice_id,hours_id) values('" . $iid . "','" . $hour->id . "')");
                } else {
                    break;
                }
            }
        }

	if($euros <= 0) {
		// delete rightafter if zero euros
		$db->query("delete from Invoices where id = '".$iid."'");
		die(json_encode(array("error"=>"zero_euros")));
	}

	$first_sentence = "";

        $res = $db->query("select * from Projects where  id = '" . ROUTE[1] . "' limit 1");
        while ($row = $res->fetch_object()) {

	    $first_sentence = "Die Rechnung R".$iid." über ".euros($euros)."€ im Projekt \"".$row->title."\" wurde erstellt";

            if($euros > 0 && isset($body["sendmail"]) &&  $body["sendmail"] == true) {
            sendmail($row->cid, "Covomedia | Neue Rechnung vom " . date("d.m.Y"), "wir bedanken uns für die gute Zusammenarbeit und senden Ihnen vereinbarungsgemäß die
Rechnung R" . $invoice_no_max . ".<br><br>Sie können die Rechnung unter diesem Link herunterladen: <br><a href='https://api.covomedia.com/pdf/?pid=" . ROUTE[1] . "&id=" . $iid . "&type=invoice'>https://api.covomedia.com/pdf/?pid=" . ROUTE[1] . "&id=" . $iid . "&type=invoice</a><br><br>
                <a href='https://api.covomedia.com/pdf/?pid=" . ROUTE[1] . "&id=" . $iid . "&type=invoice' class='btn'>Zur Rechnung &#8883;</a>", $row->uid);
		slack_msg($first_sentence." und ein entsprechender Link an den Kunden versendet. Hier der Link: https://api.covomedia.com/pdf/?pid=" . ROUTE[1] . "&id=" . $iid . "&type=invoice");
		die(json_encode(array("success"=>true,"sendmail"=>true)));
}
        }

	slack_msg($first_sentence.". Es wurde KEINE Email an den Kunden versendet. Hier der Link: https://api.covomedia.com/pdf/?pid=" . ROUTE[1] . "&id=" . $iid . "&type=invoice");
	die(json_encode(array("success"=>true,"sendmail"=>false)));

        break;
    case "DELETE":
        require_policy("invoices_delete");
        $sql = "delete from Invoices where id = '" . ROUTE[3] . "'";
        return_query_success($sql);
        $sql = "delete from InvoiceHasHours where invoice_id = '" . ROUTE[3] . "'";
        return_query_success($sql);
        break;
    case "PUT":
	
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
	
        require_policy("invoices_edit");
        if ($body["status"] == "paid") {

            $body["ts_paid"] = date("Y-m-d H:i:s");

            $res = $db->query("select * from Invoices where  id = '" . ROUTE[3] . "' limit 1");
            $invoice = $res->fetch_object();

	    $sendmail_str = "Es wurde KEINE Email an den Kunden gesendet. ";
            $res = $db->query("select * from Projects where id = '" . $invoice->pid . "' limit 1");
            while ($row = $res->fetch_object()) {
            if(isset($body["sendmail"]) &&  $body["sendmail"] == true)
		$sendmail_str = "Es wurde eine entsprechende Email an den Kunden gesendet. ";
                sendmail($row->cid, "Covomedia | Vielen Dank, wir haben Ihre Zahlung erhalten", "vielen Dank für Ihre Überweisung. Hiermit bestätigen wir Ihnen den Zahlungseingang für die Rechnung R" . $invoice->invoice_no . ".<br><br>
		Sie können die Rechnung für den Zeitraum von 90 Tagen weiterhin unter diesem Link einsehen: <br><a href='https://api.covomedia.com/pdf/?pid=" . $invoice->pid . "&id=" . ROUTE[3] . "&type=invoice'>https://api.covomedia.com/pdf/?pid=" . $invoice->pid . "&id=" . ROUTE[3] . "&type=invoice</a><br><br>
                <a href='https://api.covomedia.com/pdf/?pid=" . $invoice->pid . "&id=" . ROUTE[3] . "&type=invoice' class='btn'>Zur Rechnung &#8883;</a>", $row->uid);
            }

	    slack_msg("Zahlungseingang für Rechnung R" . $invoice->invoice_no .". ".$sendmail_str."Hier der Link: https://api.covomedia.com/pdf/?pid=" . ROUTE[1] . "&id=" . ROUTE[3] . "&type=invoice");
        }
        $sql = "update Invoices " . get_update_phrase($body) . " where id = '" . ROUTE[3] . "'";
        return_query_success($sql);
        break;
}
