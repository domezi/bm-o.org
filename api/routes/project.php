<?php
switch (METHOD) {
    case "POST":
        require_policy("projects_create");
	$res=$db->query("select * from Users where id = '".UID."'");
	$wage = $res->fetch_object()->wage;
        $sql = "INSERT INTO `Projects` (`id`, `title`, `slug`, `uid`, `cid`, `iss`, `type`, `status`) VALUES (NULL, '" . $body["title"] . "', '" . strtoupper($body["slug"]) . "','" . UID . "', '" . $body["cid"] . "', CURRENT_TIMESTAMP,'" . $body["type"] . "','draft')";
        require_success($sql);
	$out["insert_id"]=$db->insert_id;
	$sql = "insert into ProjectHasWage(pid,uid,wage) values('".$db->insert_id."','".UID."','".$wage."')";
        require_success($sql);
	
	// create slack channel
	slack_action("channels.create",array("name"=>strtolower($body["slug"])));
	slack_action("chat.postMessage",array("text"=>"Projekt \"".$body["title"]."\" wurde erstellt. ","channel"=>strtolower($body["slug"])));


        break;
    case "DELETE":
        /*
        if (ROUTE[1] == PID)
            done(420);
            */
        require_policy("projects_delete"); // kann sämtliche projects löschen
        return_query_success("update Projects set status = 'trash' where id = '" . ROUTE[1] . "'");
        break;
    case "PUT":

        require_policy("projects_edit");

        $sql = "update Projects " . get_update_phrase($body) . " where id = '" . ROUTE[1] . "'";
        return_query_success($sql);
        break;
    case "GET":

	/* get the whole project - dependent data */

	// project
	$out["project"] = [];
	$res = $db->query("select * from Projects where id = '" . UID . "' limit 1");
	if ($res->num_rows) {
		while ($row = $res->fetch_assoc()) {
			$out["project"] = $row;
		}
	}

	// everything else

	$success = true;

	// hours
	$out["hours"] = [];
	$out["tasks"] = [];
	if(has_policy("hours_list")) {
		$res = $db->query("select h.*,i.status from Hours h left join InvoiceHasHours ih on ih.hours_id = h.id left join Invoices i on i.id = ih.invoice_id where h.pid = '" . ROUTE[1] . "' order by iss desc");
		if ($res->num_rows) {
			while ($row = $res->fetch_assoc()) {
				$row["ttl"] = time()-strtotime($row["iss"]);
				$out["hours"][] = $row;
			}
		}


		// tasks	
		$res = $db->query("select * from Tasks where pid = '" . ROUTE[1] . "' order by iss desc");
		if ($res->num_rows) {
			while ($row = $res->fetch_assoc()) {
				$out["tasks"][] = $row;
			}
		}
	}

	// wages
	$out["wages"] = [];
	if(has_policy("wages_list")) {
		$res = $db->query("select * from ProjectHasWage where pid = '" . ROUTE[1] . "'");
		if ($res->num_rows) {
			while ($row = $res->fetch_assoc()) {
				$out["wages"][] = $row;
			}
		}
	}

	// invoices
	$out["invoices"] = [];
	if(has_policy("invoices_list")) {
		if(intval(ROUTE[1]) == 0 && has_policy("brutto_list"))

			$res = $db->query("select * from Invoices where 1 order by iss desc");
		else
			$res = $db->query("select * from Invoices where pid = '" . ROUTE[1] . "'");
		if ($res->num_rows) {
			while ($row = $res->fetch_assoc()) {
				$row["fixeds"] = [];
				$fixeds = $db->query("select Hours.* from InvoiceHasHours,Hours where InvoiceHasHours.hours_id = Hours.id and InvoiceHasHours.invoice_id = '" . $row["id"] . "'");
				if ($res->num_rows) {
					while ($fixed = $fixeds->fetch_assoc()) {
						$row["fixeds"][] = $fixed;
					}
				}
				$out["invoices"][] = $row;
			}
		} 
	} 

	// quotes
	$out["quotes"] = [];
	if(has_policy("quotes_list")) {
		$res = $db->query("select * from Quotes where pid = '" . ROUTE[1] . "'");
		if ($res->num_rows) {
			while ($row = $res->fetch_assoc()) {
				$row["fixeds"] = [];
				$fixeds = $db->query("select * from QuoteHasFixed where qid = '" . $row["id"] . "'");
				if ($res->num_rows) {
					while ($fixed = $fixeds->fetch_assoc()) {
						$row["fixeds"][] = $fixed;
					}
				}
				$out["quotes"][] = $row;
			}
		} 
	}	

	// brutto
	$out["brutto"] = [];
	if(has_policy("brutto_list")) {
		$res = $db->query("select * from Invoices where 1 order by iss desc");
		if ($res->num_rows) {
			while ($row = $res->fetch_assoc()) {
				$row["fixeds"] = [];
				$fixeds = $db->query("select Hours.* from InvoiceHasHours,Hours where InvoiceHasHours.hours_id = Hours.id and InvoiceHasHours.invoice_id = '" . $row["id"] . "'");
				if ($res->num_rows) {
					while ($fixed = $fixeds->fetch_assoc()) {
						$row["fixeds"][] = $fixed;
					}
				}
				$out["brutto"][] = $row;
			}
		} 


		$res = $db->query("select max(iss) as iss_max, min(iss) as iss_min,sum(euros) as euros,sum(mins) as mins,pid from Hours h left join InvoiceHasHours i on i.hours_id = h.id where pid != 0 and hours_id is null group by pid having mins > 0 or euros > 0 order by iss_max asc");
		if ($res->num_rows) {
			while ($row = $res->fetch_assoc()) {
				$out["hoursQueue"][] = $row;
			}
		}


	} 

	$data = true;
        break;
}
