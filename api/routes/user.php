<?php
switch (METHOD) {
    case "POST":
        require_policy("users_create");

        if (!has_policy("policies_edit"))
            unset($body["policies"]);

        error_if_res("select * from Users where email like '" . str_replace("_", "\_", $body["email"]) . "'");
        $sql = "INSERT INTO `Users` (policies,`id`, `fname`, `lname`, `pass`, `email`) VALUES ('null',NULL, '" . $body["fname"] . "', '" . $body["lname"] . "', '" . hash_pass($body["pass"]) . "', '" . $body["email"] . "')";
        return_query_success($sql);
        break;
    case "DELETE":
        if (ROUTE[1] == UID)
            done(420);
        require_policy("users_delete"); // kann sämtliche user löschen
        require_success("delete from Logins where uid = '" . ROUTE[1] . "' and uid != '" . UID . "'");
        return_query_success("delete from Users where id = '" . ROUTE[1] . "'");
        break;
    case "PUT":

        if (isset($body["pass"]) && strlen($body["pass"])>0)
            $body["pass"] = hash_pass($body["pass"]);

        if (ROUTE[1] != UID)
            require_policy("users_edit");

        if (!has_policy("policies_edit"))
            unset($body["policies"]);

        $sql = "update Users " . get_update_phrase($body) . " where id = '" . ROUTE[1] . "'";
        return_query_success($sql);
        break;
    case "GET":
        $sql = ("select id,fname,lname,email,'' as pass,policies,wage,bankdetails,recent_pid,secret,slack_id from Users where id = '" . UID . "' limit 1");
	$res = $db->query($sql);
	if($res->num_rows) {
		$data = $res->fetch_assoc();
		$data["secret"] = (strlen($data["secret"])===0) ? "" : "*";
		$data["slack_id"] = (strlen($data["slack_id"])>1) ? "*" : "";
	} 

        break;
}
