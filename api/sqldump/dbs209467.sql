-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: db5000214720.hosting-data.io
-- Generation Time: Feb 17, 2020 at 09:37 AM
-- Server version: 5.7.28-log
-- PHP Version: 7.0.33-0+deb9u6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbs209467`
--

-- --------------------------------------------------------

--
-- Table structure for table `Customers`
--

CREATE TABLE `Customers` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `sex` enum('w','m','o') NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'created by uid'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Customers`
--

INSERT INTO `Customers` (`id`, `title`, `sex`, `address1`, `address2`, `email`, `phone`, `iss`, `fname`, `lname`, `uid`) VALUES
(5, 'Dr. ', 'm', '', '', 'dominik.ziegenhagel@yahoo.de', '', '2019-11-08 09:16:52', 'Max', 'Mustermann', 0),
(6, 'Dr. ', 'w', 'Beispielstrasse 3', '79123 Arendsese', 'dominik.ziegenhagel@gmail.com', '', '2019-11-08 19:21:18', 'Maya', 'Schenk', 6),
(7, '', 'w', 'Annenstr 1', '09405 Annaberg', 'dominik.ziegenhagel@mailbox.tu-dresden.de', '00097654', '2019-12-16 23:13:28', 'Anna', 'Anni', 35),
(8, '', 'm', 'Zum Googlehupf 1', 'Amazonien 901234', 'dominik.ziegenhagel@gmail.com', '01234543210', '2020-02-16 18:17:09', 'Peter', 'Arbeitsloser', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Expenses`
--

CREATE TABLE `Expenses` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'von wem eingetragen/ausgelegt?',
  `euros` double NOT NULL,
  `status` enum('refunded','refund_rejected','open') NOT NULL COMMENT 'wurde das geld dem auslegenden zurückerstattet?',
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  `invoice_uri` text NOT NULL,
  `tag` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Expenses`
--

INSERT INTO `Expenses` (`id`, `uid`, `euros`, `status`, `iss`, `description`, `invoice_uri`, `tag`) VALUES
(0, 1, 9, 'open', '2019-11-27 11:12:36', 'covomedia.de registrierungsgebühr', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `Files`
--

CREATE TABLE `Files` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `iss` int(11) NOT NULL,
  `type` enum('quote','invoice','other') NOT NULL,
  `uid` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Hours`
--

CREATE TABLE `Hours` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `uid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `iss` datetime DEFAULT CURRENT_TIMESTAMP,
  `mins` int(11) NOT NULL COMMENT 'minutes worked, or zero if fixed price',
  `euros` double NOT NULL,
  `tid` int(11) DEFAULT '0' COMMENT 'zero if not assigned task'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Hours`
--

INSERT INTO `Hours` (`id`, `text`, `uid`, `pid`, `iss`, `mins`, `euros`, `tid`) VALUES
(1, '', 30, 0, '2019-11-11 15:14:42', 0, 0, 0),
(2, 'test', 6, 0, '2019-11-11 19:11:39', 3, 4, 0),
(5, 'test2', 6, 0, '2019-11-11 19:12:39', 240, 20, 0),
(6, 'frisch gemacht', 6, 0, '2019-11-11 20:35:16', 10, 10, 0),
(7, 'test', 6, 0, '2019-11-12 00:04:57', 0, 0, 0),
(8, 'RTest', 6, 4, '2019-11-12 00:06:42', 0, 0, 0),
(9, 'Kundencetner program für 3', 6, 4, '2019-11-12 00:07:42', 261, 174, 0),
(11, 'ttest', 30, 7, '2019-11-12 23:28:11', 0, 0, 0),
(12, 'Bla', 34, 8, '2019-11-13 08:51:56', 0, 0, 0),
(13, '', 0, 0, '2019-11-14 11:47:47', 0, 0, 0),
(14, 'Studioassi Update', 6, 9, '2019-11-14 18:41:49', 0, 80, 0),
(15, 'Blub', 6, 9, '2019-11-14 18:41:49', 0, 10, 0),
(16, 'Bla', 6, 9, '2019-11-14 18:41:49', 0, 5, 0),
(17, 'Studioassi Stimen eingeben', 6, 9, '2019-11-14 18:41:49', 0, 17, 0),
(18, 'rfe', 6, 9, '2019-11-14 18:41:49', 0, 32, 0),
(19, 'afsd', 6, 9, '2019-11-14 18:41:49', 0, 5513, 0),
(20, 'fdsa', 6, 9, '2019-11-14 18:41:49', 0, 1230, 0),
(21, 'dsf', 6, 9, '2019-11-14 18:41:49', 0, 432, 0),
(29, 'Studioassi Update 2', 6, 9, '2019-11-14 18:51:34', 0, 80, 0),
(30, 'Blub', 6, 9, '2019-11-14 18:51:34', 0, 10, 0),
(31, 'Bla', 6, 9, '2019-11-14 18:51:34', 0, 5, 0),
(32, 'Studioassi Stimen eingeben', 6, 9, '2019-11-14 18:51:34', 0, 17, 0),
(33, 'rfe', 6, 9, '2019-11-14 18:51:34', 0, 32, 0),
(34, 'afsd', 6, 9, '2019-11-14 18:51:34', 0, 5513, 0),
(35, 'fdsa', 6, 9, '2019-11-14 18:51:34', 0, 1230, 0),
(36, 'dsf 2', 6, 9, '2019-11-14 18:51:34', 0, 432, 0),
(44, 'Studioassi Update 23', 6, 9, '2019-11-14 18:53:24', 0, 80, 0),
(45, 'Blub', 6, 9, '2019-11-14 18:53:24', 0, 10, 0),
(46, 'Bla', 6, 9, '2019-11-14 18:53:24', 0, 5, 0),
(47, 'Studioassi Stimen eingeben', 6, 9, '2019-11-14 18:53:24', 0, 17, 0),
(48, 'rfe', 6, 9, '2019-11-14 18:53:24', 0, 32, 0),
(49, 'afsd', 6, 9, '2019-11-14 18:53:24', 0, 5513, 0),
(50, 'fdsa', 6, 9, '2019-11-14 18:53:24', 0, 1230, 0),
(51, 'dsf 2', 6, 9, '2019-11-14 18:53:24', 0, 432, 0),
(62, 'Studioassi Stimen eingeben', 6, 9, '2019-11-14 18:53:57', 0, 17, 5),
(63, 'rfe', 6, 9, '2019-11-14 18:53:57', 0, 32, 5),
(64, 'afsd', 6, 9, '2019-11-14 18:53:57', 0, 5513, 0),
(65, 'fdsa', 6, 9, '2019-11-14 18:53:57', 0, 1230, 0),
(66, 'dsf 2', 6, 9, '2019-11-14 18:53:57', 0, 432, 0),
(74, 'test', 6, 4, '2019-11-15 10:23:20', 20, 13.33, 3),
(75, 'fdsalkfdsa', 6, 4, '2019-11-15 13:03:23', 130, 86.67, 0),
(76, 'fdsalkfdsa', 6, 4, '2019-11-15 13:03:23', 130, 86.67, 0),
(77, 'fdsalkfdsa', 6, 4, '2019-11-15 13:03:23', 130, 86.67, 0),
(78, 'fixpreis 110uro', 6, 4, '2019-11-15 13:03:46', 0, 110, 0),
(79, 'fixpreis 110uro', 6, 4, '2019-11-15 13:03:46', 0, 110, 0),
(81, 'fdsaf', 6, 4, '2019-11-15 18:13:59', 32, 21.33, 0),
(83, 'fdsa', 6, 4, '2019-11-15 18:14:07', 32, 21.33, 0),
(84, 'fdsa', 6, 4, '2019-11-15 18:14:07', 32, 21.33, 0),
(85, 'tesa', 6, 9, '2019-11-17 16:17:06', 3, 1, 5),
(86, 'fdsa', 6, 9, '2019-11-17 16:17:31', 5, 5, 5),
(87, 'Planen', 35, 12, '2019-12-16 23:21:20', 30, 20, 0),
(88, 'Nachbesserungen', 1, 13, '2020-02-16 18:22:10', 120, 40, 0),
(89, 'Kleinigkeiten', 1, 13, '2020-02-16 18:22:47', 20, 15.33, 0),
(90, 'Festpreis nach AG63', 1, 13, '2020-02-16 18:27:28', 0, 1710, 0);

-- --------------------------------------------------------

--
-- Table structure for table `InvoiceHasHours`
--

CREATE TABLE `InvoiceHasHours` (
  `invoice_id` int(11) NOT NULL,
  `hours_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `InvoiceHasHours`
--

INSERT INTO `InvoiceHasHours` (`invoice_id`, `hours_id`) VALUES
(35, 8),
(35, 9),
(35, 74),
(35, 75),
(35, 76),
(35, 77),
(35, 78),
(35, 79),
(35, 80),
(35, 81),
(35, 82),
(35, 83),
(35, 84),
(38, 14),
(38, 15),
(38, 16),
(38, 17),
(38, 18),
(38, 19),
(38, 20),
(38, 21),
(38, 29),
(38, 30),
(38, 31),
(38, 32),
(38, 33),
(38, 34),
(38, 35),
(38, 36),
(38, 44),
(38, 45),
(38, 46),
(38, 47),
(38, 48),
(38, 49),
(38, 50),
(38, 51),
(38, 62),
(38, 63),
(38, 64),
(38, 65),
(38, 66),
(38, 85),
(38, 86),
(39, 88),
(39, 89),
(39, 90);

-- --------------------------------------------------------

--
-- Table structure for table `Invoices`
--

CREATE TABLE `Invoices` (
  `id` int(11) NOT NULL COMMENT 'rechnungsnr zb 1902 (2019 2nd invoice)',
  `pid` int(11) NOT NULL,
  `status` enum('awaiting_payment','paid') NOT NULL,
  `iss` datetime NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `ts_paid` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Invoices`
--

INSERT INTO `Invoices` (`id`, `pid`, `status`, `iss`, `invoice_no`, `uid`, `ts_paid`) VALUES
(1, 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00'),
(35, 4, 'paid', '2019-11-17 11:39:11', 191101, 6, '2019-12-16 23:05:37'),
(36, 12, 'paid', '2019-12-16 23:23:44', 191201, 35, '2019-12-16 23:25:52'),
(38, 9, 'paid', '2020-02-16 18:25:40', 200201, 1, '2020-02-16 18:26:31'),
(39, 13, 'paid', '2020-02-16 18:27:44', 200202, 1, '2020-02-16 18:28:58');

-- --------------------------------------------------------

--
-- Table structure for table `Logins`
--

CREATE TABLE `Logins` (
  `uid` int(11) NOT NULL,
  `hash` varchar(128) NOT NULL,
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Logins`
--

INSERT INTO `Logins` (`uid`, `hash`, `iss`) VALUES
(6, '0170a70044060cf86c1d9aed8c12bdf7403fa0a92e7db03d85beab22dd9ba2a3948c224bf66098326b6ebb423eae6746db12304b2a0ebf99e9003262efb302a5', '2019-11-06 16:51:42'),
(6, '01b1baab08718f06926274d56168c0469789929a10c21fdf5e12bf97ed61bf0f9140d11725970e83e30d888dd4612abebffab62158e5f4dbd72e41dae4d3a77b', '2019-11-26 14:40:32'),
(6, '01e294507ce9da632eb50a76374b69eea443fa2c64d4d983fdff2e7cf77e6f2757aca18a00be7ef8d4f4e03044c424031a333984f20cbc7b0a3d8db5c0274d86', '2019-11-14 14:17:44'),
(1, '09969aca3cb228a3202b71a72d582b3a495eaf77dd67f9ec0150ddeb2c1672374ba97e9ef9fb71c277937865ef28c0090277d7917917fbae945d819df5ae6650', '2020-02-16 18:12:26'),
(35, '0a3ad969f7072c0eb54e6d7a267f11670454f26ae62f063ea79fb10e06547f815c54248da9365990d2169b40b76d0a80010c3b0651bbeb91b4b2e93c580c63bd', '2019-11-17 17:14:50'),
(34, '0bb2e7b91d8665ef96f7d0ab14f2cefb139c580c05a6f1e538ed9be60807cec49f724f68fff9287d9df9c69669b1e6d7665837031ebede79a8f7106853613fb1', '2019-11-12 23:32:57'),
(34, '0c5424ae074e315c7981d7b9bd34e458267caa4b2a89befbe824873de434638b5cac464fe054f8c09a9e3bd92845ac1ff3513db3f1177137682e2d6494ef6132', '2019-11-13 08:52:55'),
(6, '13e64280e5f5e4599e056f8f10c615e61e1fdc65c685b77a49837ee18208c57d879316a7c54ac2c4f73b77e6b90d8a9500e249cc813568ca96fc52376b884600', '2019-11-14 10:21:06'),
(6, '1448faabe30d91702b374ef603f6e4e012466ee5ebc3716604f1f15e0bb6a6b7d493aaa613303de961e544c7e1364dee6c58058cd27b946f8fbdefd38269893e', '2019-11-12 23:07:52'),
(6, '1aea35f15209cbf2c01bfa7ad59d223f6461344a0486874d4edc226b03253b73763c2cbb28f3a4df24be79f25c0b18490ea28d14afa7352a9b6e9b3a486cd5a9', '2019-11-15 17:23:09'),
(1, '1d5c59644f2b8c4da2b3f9e5b1a4bf1c683badda90ef0a8c4c4418f6613eebe9b40afbdc3f0346a4bd612114953324f5d25fa0f4d622af369efff725d44f3065', '2019-12-16 22:48:45'),
(6, '1e9b7afe7872c6ac5cf9cdc71b837ed23e708e9b8760a51ee07d5b47e271c9412df79b99e1e92815767b997547438f1d01863af36d20cf3647af5f0116412523', '2019-11-17 10:33:35'),
(35, '2105fcdd62e0beff5fafb043a8308af5f729cff00c2aea275efcef953bfa63726787a58e3afb5b7474f3ecf19b1427526949dbbbd48520eca555e25ccc55b909', '2019-12-16 23:10:04'),
(34, '23bec8cd51be3701174e3b534b5288eb158c31e520762e5a99b29b6a82c8738525c85162b943890dd20ff8f72c239ae730f874b5bc6f0571ee9105a8fe8c1914', '2019-11-12 23:37:52'),
(34, '2628da20eea6b03ad6fb234ac584c79831274179d5c09f1d4fc32e7a914214123afade70ca0455a090001b0e6cb38f2be539dc0bd42f78a35196acf89bbd231f', '2019-11-13 08:53:46'),
(6, '28afd47b8a845511d0552f980be926691992e43cdef24fe0c24c77a4d9aead0170914e11918232012879b97201a7d934a31d1c664479e6cc674458313fa57a7f', '2019-11-12 23:14:00'),
(6, '2d83eaf21f313d060736868c275a76ea067f25f909570cc3c4e66c8b152e1be9000f664d7f089b6c4a119ae9b4fc66ab8d1e4f40655dea40ed7a885e111dfb9e', '2019-11-14 18:50:08'),
(6, '2e64079103ba930859c7395abf8db48cbb840ddadb623a614d80c443013835b10688fc9eb0b6f5e974a0b64d4ea3178b6969b5bce190ecccb1280216ca5f8451', '2019-11-12 23:14:04'),
(6, '34619c4915d35802b8987c00d62d6bb29046a10f9264b63a554a857d6c904194f57b3e7b33f405ea9cce3f04963c15f6056561487c0fb81d9edee108a6ccf794', '2019-11-06 16:20:47'),
(6, '3548884d3301cb300dc01e3e0b59e4d7ee2d7e26ee6e5399cc9cabf66d244a150176313faa6a652e0bdd0a12dba9ec8689eb12aed508cf44ae4473153e1a2eda', '2019-11-12 23:19:33'),
(6, '364e6239768a1704e13a6845af07b2c94751d5af888e809a73178302e4bbe1afd18851e81136b715e006647505d284e17b883731f5ac14e016fa678898749ad7', '2019-11-16 17:53:05'),
(34, '3937f33f4fd09a5c59a2246241e2183124c99378dc70254411dd7dbf43c0bcaf09bed1ad6dee0c19f9aa527bc6c9956ba3bf76e650d4f197274324be0a71dc1e', '2019-11-15 14:08:24'),
(6, '39dd6c9a8c4296b040f4dba5273dfb0950edfe306285b0ab1aad53824066c427aad5c926dc4a5d75f278a725a2632f1484565117dc92795f643e37600618a6a3', '2019-11-16 18:08:26'),
(1, '3f781fc8bea107ec0e9bb2f0b79aded13a019be221104cd9a05aefebdff9f0d634919ccb47547353de512477ceb2eff5cd0a2c2c04bb50e8d8243a5762c97408', '2019-12-02 23:24:02'),
(6, '4381cf4184638504ecf4c059720c2f1d7a90bab2d35ad96d3521440a9fa1610c50eb69e8867750dafbdcb9a4703f0fa730c302399744ef359079d4d59870d29d', '2019-11-12 23:07:49'),
(6, '489694c36faa6758954466a4caab3dfe8225433c1f9ba71598641b5a49f5990355829bf03f62b7723728ab832f2bb9398d75d343c1029a0c853b00fcec0e609a', '2019-11-17 11:35:22'),
(6, '4f737f91d645d638764d86982ac274855e3353deab74b929457f7f340a12a61534279382141ee72d69aa79c5a892974ec761fe5cac8db7b1de494d8dfcf807e7', '2019-11-06 16:51:34'),
(6, '5becd5fcd13ca9fcee1842c64cd03541e9506ba99b50ee0777c71303061016b9b90bb42fecf76b00663b4826f377d63438372982d243d56b2eeb79c162d7528b', '2019-11-06 16:21:00'),
(6, '5f37af830fb137ae775b4da5046899c8aaaa6a3b5e2aaf7008a216820b5ee0a6ed5231d7a4ccd0b7a20734da994a92c1e9bb23c8baaf150833bde823e59638f5', '2019-11-14 10:25:51'),
(6, '6100941f554ef72a09f9a158d27148eda384c19dcdd8a995e5421ce24395c52ce1e13fb03f000f589c775aaae493eeafca37f2b92fb3ee84b78d1d2b323e0d1e', '2019-11-26 12:43:00'),
(6, '6442d1fbf35548e408b4174f288cfc958e07f7909c749985ae69c4eb3e654db80ac0082c10ba4ae686336279f4d10d13bdbfc9eb1b667c9c6d2422f385058e77', '2019-11-14 18:52:45'),
(6, '66879512d40030b281b8e84fbb80738a9d4aeb6eae14d2d570c3e2a14886ae4706e09118ddfccb06f0906d4961d1930deeeeb625b779f42aeec291b84e6d547d', '2019-11-12 23:07:23'),
(6, '80d884ab17dc75b635a337002be0ed706c760534ee39a3c26973711116886f6b50dafea608e2d242ef804ee08c5c121cdba6ba65c588dea1f2157044635521c9', '2019-11-14 12:32:38'),
(6, '84a0c6919a2933e6c4c6ddf245207411bd3c0dc62541bbd67bbcefd13e299daad9bae65b403b176cba185a02392f1b4147571c9740fe05444efd3f6ffb182a0d', '2019-11-26 19:31:06'),
(6, '914b1534653519597638c4ef5a5b5f9fb12d67258318a90417a6228c8a17e3c363395e650046f4ac244d7b31c1bbb7ca9554000a34f4b7bcd51a1a997f4acd2f', '2019-11-17 11:35:27'),
(6, '9930c27200a45fbe7ac7bd82a20216d47f2549487bfb3b81f21205136795ecfcaf4f0f0ca94de5fa12d35a5132bfb8f752f4931ed34008674e85d745e70f6f4c', '2019-11-12 23:07:51'),
(6, '9a105dde39889e3c1d8c25343b54f4579a8050c608ce497011c6b7a5a9e4583c518523f9a22a8a64650039dec7d62fba3e30ed40b9e3d99b888dc564d52034f1', '2019-11-13 09:28:19'),
(1, '9f576f0804fafa76e119ee4344dfe7e6d5385d11807b57d7161a18cdb7b83c5b0efef6ad3a4b25519be44624430c2f7815128bbcc56de6a13eb358461b5e3acc', '2019-12-16 23:10:08'),
(6, 'a0764bd70135a09215b1f43a3c258bf38aa36ea94a4a1ef3ce0e0b1c539ac358378fc5616c98ce1e628af665ae168f13f098f3bd65c2994608290c615d85b1bf', '2019-11-26 14:40:32'),
(6, 'aa9b2ecba188545d8712771e664da701f2ff7ab0849c5e5febab5f2538432f7a1ced2169f37332b90cbe6ee9be1a7b645d9776dfc2ed80201dc2b30c52f94927', '2019-11-16 18:40:05'),
(6, 'ab2005735b20041b6eff999b2f8f14a7ea4612acc1fc3ce8621b4f4d5d2c43213add7980aaab831023a871d0554cc772ce07d4552edfe91e89654cc13c1e8258', '2019-11-06 16:51:43'),
(6, 'afe4550bdc7ddc5378fa57d41bc8ae8d8daf91c9c509f4bca9bafc72ab2d3b2fcf08ce8c7b3488fe1360a5f2c6bf267c8002e7296b0367861b1c8092045ea37c', '2019-11-17 16:03:33'),
(6, 'c25da14aab60e36b8fb10f2d5f9697cc9b56f98bc057a6b4105d70c6c5f9fe8ad3edfc9de9d361cbb811d4c4311d21fe1151787a3fc713fdb7416d238f3c214c', '2019-11-26 19:31:25'),
(6, 'c51d1465b385dc69df23afac6e4711be5b81dbaa0b6119142a84a1096d84cf4939606fc3871ada7cb69bb95d6a7810d40a157bb43cc05acea402c7d7c984798a', '2019-11-14 12:26:50'),
(34, 'cb11d1c95da79b9b024be648b58983747f1eace362e499407ad9fb36083ffa685c61e8cd7e175cdb3a5e59e484247753447b32b9b66966f39f447063b030754a', '2019-11-13 08:49:44'),
(6, 'cc1191a9d178440ea0c5ff0339d4d33301fbf1df629cc715a3ceacbea7962938cb2698116ec78e4c6596400f29ac1b28e89e5f6d996cfb6040850d58fcdbb8bf', '2019-11-12 23:13:37'),
(6, 'd375b23537f12ef5fa6571f6ec6a3c3346ff4a2574c12632d0e3aad116ac0044cecf7a312efa84acedcfbbb371b77af78a090e61a6fd664646a395fb77a4215f', '2019-11-12 23:11:47'),
(34, 'd9cf16261cad53598d2a6c686f4e3d842d86e2d90d919a7053b1d1cff990c1dabcc0c281ba9145ae780538f4e3e0cfad609c2e5640574209453b07903a0a595d', '2019-11-13 09:29:40'),
(6, 'e7ad000e6ef265a9a6e13ef5768a24574b63e73b95595b43b7928a5c35729863605243714500d8b1d60d45f085c728d7d25d55fa68be35b9acc8ef5651d72ba4', '2019-11-12 23:08:09'),
(1, 'ea1d4319381836fcae5aaf3c1e39bc0ec914740210762778be747d0aac81273f95d107d1e455652220a1d7e462fe1d65b5133e3d8102cc72965a21e2aabac421', '2020-02-16 18:13:23'),
(34, 'ea6c7fff08b14767a6d3d44a599b55be6a42c5bae681989884c1cfbe44149df66607fb5af03591fe59fc13b18d79c173566bf73f81294528d2d4d53d76397d60', '2019-11-13 08:49:58'),
(6, 'ef2d0551b17b62e01881e923a5f1950019997d767e788f5209bf5f6f8b7f44a6ebd1567d491abe564b2f3eb5330b40b8617174d0d0e4cb30cbd9c9ce9ef86ae2', '2019-11-14 18:52:45'),
(6, 'f50aabfd6a520022f1852b38b8d026b0d1f209aaf3766c6e27f287c70a5a386c943be3f6a538430a205dccc5627e973cb04e7974220be56a97b720dce2c94c37', '2019-11-06 16:50:43'),
(1, 'fad2aac1efd8c6e77e116f5f03d6ac77b81655fa027f519f088daf4b90de08fe8f083e56e76a98ac9308b9b38dd85e7e189b614d3bdd10cee4a22891cd7b5098', '2019-12-16 22:49:16'),
(6, 'fed806ee4d20955190fc5068fd27072ca60bd505538d7174250f9f04a7f0309f1e5eec960b53a503d1ba41103cf309b78fce5e12768f5b01d623d55b6fcbb12c', '2019-11-15 17:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `Policies`
--

CREATE TABLE `Policies` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `policy` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Policies`
--

INSERT INTO `Policies` (`id`, `title`, `policy`) VALUES
(1, 'Superuser', '{\"*\": \"*\"}'),
(6, 'Mitarbeiterverwaltung', '{\"users\": \"*\"}');

-- --------------------------------------------------------

--
-- Table structure for table `ProjectHasWage`
--

CREATE TABLE `ProjectHasWage` (
  `pid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `wage` double NOT NULL COMMENT 'euro pro stunde'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ProjectHasWage`
--

INSERT INTO `ProjectHasWage` (`pid`, `uid`, `wage`) VALUES
(4, 6, 40),
(4, 29, 35),
(7, 30, 12),
(8, 34, 16),
(9, 34, 16),
(12, 1, 20),
(12, 35, 40),
(13, 1, 46);

-- --------------------------------------------------------

--
-- Table structure for table `Projects`
--

CREATE TABLE `Projects` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'created by userid',
  `cid` int(11) NOT NULL COMMENT 'customer id',
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('imagefilm','website','other') NOT NULL,
  `status` enum('draft','archive','active','trash') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Projects`
--

INSERT INTO `Projects` (`id`, `title`, `slug`, `uid`, `cid`, `iss`, `type`, `status`) VALUES
(1, '\r\n', '', 0, 0, '2019-11-07 18:02:34', '', 'trash'),
(2, '12fdsa3', 'FJALKD', 6, 0, '0000-00-00 00:00:00', 'website', 'trash'),
(3, 'Testmax', 'TESTMA', 6, 5, '2019-11-08 12:36:54', 'website', 'trash'),
(4, 'third project #3', 'THIRDP', 6, 5, '2019-11-08 19:04:51', 'website', 'draft'),
(5, '323232323', '332323', 6, 0, '2019-11-08 19:06:08', 'website', 'trash'),
(6, 'test3', 'TEST41', 6, 0, '2019-11-08 19:18:19', 'website', 'trash'),
(7, 'Testproject', 'TESTPR', 6, 0, '2019-11-10 13:01:40', 'website', 'draft'),
(8, 'Test', '', 34, 0, '2019-11-12 23:37:59', 'website', 'draft'),
(9, 'Lochmeister', 'JFC', 34, 5, '2019-11-13 09:08:25', 'imagefilm', 'draft'),
(10, 'blub', '', 34, 0, '2019-11-13 09:32:45', 'website', 'trash'),
(11, '', '', 6, 0, '2019-11-15 11:59:16', '', 'draft'),
(12, 'Bettbau', 'ANAN', 35, 7, '2019-12-16 23:17:08', 'website', 'draft'),
(13, 'The Shop', 'THSHOP', 1, 8, '2020-02-16 18:17:41', 'website', 'draft');

-- --------------------------------------------------------

--
-- Table structure for table `QuoteHasFixed`
--

CREATE TABLE `QuoteHasFixed` (
  `id` int(11) NOT NULL,
  `euros` double NOT NULL,
  `qid` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `QuoteHasFixed`
--

INSERT INTO `QuoteHasFixed` (`id`, `euros`, `qid`, `text`) VALUES
(1, 0, 0, ''),
(2, 0, 16, ''),
(3, 0, 16, ''),
(4, 0, 16, ''),
(5, 0, 16, ''),
(6, 0, 16, ''),
(7, 0, 16, ''),
(8, 0, 16, ''),
(9, 0, 16, ''),
(10, 0, 16, ''),
(11, 0, 16, ''),
(12, 0, 16, ''),
(13, 0, 16, ''),
(14, 0, 16, ''),
(15, 0, 16, ''),
(16, 0, 16, ''),
(17, 0, 16, ''),
(18, 0, 16, ''),
(20, 0, 17, ''),
(21, 0, 17, ''),
(22, 0, 17, ''),
(23, 0, 17, ''),
(24, 0, 17, ''),
(25, 0, 17, 'fdsa'),
(26, 0, 17, ''),
(27, 0, 17, ''),
(28, 0, 17, ''),
(29, 0, 17, ''),
(30, 0, 17, ''),
(31, 0, 17, ''),
(32, 0, 17, ''),
(33, 0, 17, ''),
(34, 0, 17, ''),
(35, 0, 17, ''),
(36, 0, 17, ''),
(37, 0, 17, ''),
(38, 0, 17, ''),
(39, 0, 17, ''),
(40, 0, 17, ''),
(41, 0, 17, ''),
(42, 0, 17, ''),
(43, 10, 19, 'fdsa'),
(57, 50, 19, 'f'),
(58, 10, 21, 'fdsa'),
(60, 20, 22, 'test für 20 euro'),
(61, 180, 5, 'SEO Optimierung'),
(64, 30, 21, 'test für 30 euro'),
(66, 30, 22, 'test für 30 euro'),
(71, 1, 32, 'f'),
(72, 1, 36, 'f'),
(73, 1, 36, 'f'),
(74, 10, 21, 'fdsa'),
(75, 10, 31, 'fdsa'),
(76, 10, 33, 'fdsa'),
(77, 10, 34, 'fdsa'),
(78, 10, 35, 'fdsa'),
(79, 5, 41, 'Bla'),
(80, 10, 41, 'Blub'),
(81, 80, 41, 'Studioassi Update'),
(82, 80, 42, 'Studioassi Update 2'),
(83, 10, 42, 'Blub'),
(84, 5, 42, 'Bla'),
(85, 17, 42, 'Studioassi Stimen eingeben'),
(92, 32, 42, 'rfe'),
(93, 5513, 42, 'afsd'),
(94, 1230, 42, 'fdsa'),
(95, 432, 42, 'dsf 2'),
(99, 80, 54, 'Studioassi Update'),
(100, 10, 54, 'Blub'),
(101, 5, 54, 'Bla'),
(102, 17, 54, 'Studioassi Stimen eingeben'),
(103, 32, 54, 'rfe'),
(104, 5513, 54, 'afsd'),
(105, 1230, 54, 'fdsa'),
(106, 432, 54, 'dsf'),
(114, 5, 55, 'Bla'),
(115, 10, 55, 'Blub'),
(116, 80, 55, 'Studioassi Update'),
(117, 5, 56, 'Bla'),
(118, 10, 56, 'Blub'),
(119, 80, 56, 'Studioassi Update'),
(120, 5, 57, 'Bla'),
(121, 10, 57, 'Blub'),
(122, 80, 57, 'Studioassi Update'),
(123, 80, 58, 'Studioassi Update 2'),
(124, 10, 58, 'Blub'),
(125, 5, 58, 'Bla'),
(126, 17, 58, 'Studioassi Stimen eingeben'),
(127, 32, 58, 'rfe'),
(128, 5513, 58, 'afsd'),
(129, 1230, 58, 'fdsa'),
(130, 432, 58, 'dsf 2'),
(138, 80, 59, 'Studioassi Update 23'),
(139, 10, 59, 'Blub'),
(140, 5, 59, 'Bla'),
(141, 17, 59, 'Studioassi Stimen eingeben'),
(142, 32, 59, 'rfe'),
(143, 5513, 59, 'afsd'),
(144, 1230, 59, 'fdsa'),
(145, 432, 59, 'dsf 2'),
(153, 5, 42, 'Bla'),
(154, 5, 55, 'Bla'),
(155, 5, 56, 'Bla'),
(156, 5, 58, 'Bla'),
(157, 5, 59, 'Bla'),
(158, 5, 42, 'Bla'),
(159, 5, 55, 'Bla'),
(160, 5, 56, 'Bla'),
(161, 5, 58, 'Bla'),
(162, 5, 59, 'Bla'),
(163, 80, 56, 'Studioassi Update'),
(164, 80, 42, 'Studioassi Update'),
(165, 80, 58, 'Studioassi Update'),
(166, 80, 59, 'Studioassi Update'),
(167, 80, 55, 'Studioassi Update'),
(168, 1, 62, 'f'),
(169, 700, 63, 'Erstellung Landing Page'),
(170, 600, 63, 'Shopsystem inlk. WP'),
(171, 230, 63, 'Seiten & Artikeleinpflegung'),
(172, 180, 63, 'Übergabe & Einarbeitung'),
(174, 600, 64, 'Shopsystem inlk. WP'),
(175, 230, 64, 'Seiten & Artikeleinpflegung'),
(176, 180, 64, 'Übergabe & Einarbeitung'),
(180, 700, 65, 'Erstellung Landing Page'),
(181, 600, 65, 'Shopsystem inlk. WP'),
(182, 230, 65, 'Seiten & Artikeleinpflegung'),
(183, 180, 65, 'Übergabe & Einarbeitung'),
(187, 5, 66, 'Bla33'),
(188, 10, 66, 'Blub'),
(189, 80, 66, 'Studioassi Update33');

-- --------------------------------------------------------

--
-- Table structure for table `Quotes`
--

CREATE TABLE `Quotes` (
  `id` int(11) NOT NULL,
  `status` enum('draft','denied','accepted') NOT NULL,
  `title` text NOT NULL,
  `uid` int(11) NOT NULL,
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Quotes`
--

INSERT INTO `Quotes` (`id`, `status`, `title`, `uid`, `iss`, `pid`) VALUES
(19, 'accepted', 'Test', 6, '2019-11-10 18:23:18', 0),
(32, 'accepted', '3rd', 6, '2019-11-10 23:13:58', 4),
(33, 'draft', 'ff', 6, '2019-11-10 23:14:02', 0),
(34, 'draft', '', 6, '2019-11-10 23:14:11', 0),
(35, 'draft', 'f', 6, '2019-11-10 23:14:14', 0),
(37, 'draft', '', 6, '2019-11-10 23:16:02', 0),
(38, 'draft', '', 6, '2019-11-10 23:16:09', 0),
(39, 'draft', '', 6, '2019-11-10 23:16:15', 0),
(40, 'draft', '', 6, '2019-11-11 23:45:11', 0),
(41, 'accepted', 'Einfahces Angebot', 6, '2019-11-13 09:38:22', 9),
(42, 'accepted', 'Erwreitertes Angebot', 6, '2019-11-13 09:38:22', 9),
(43, 'draft', '', 6, '2019-11-13 18:51:59', 0),
(44, 'draft', '', 6, '2019-11-13 18:51:59', 0),
(45, 'draft', '', 6, '2019-11-13 18:52:05', 0),
(55, 'accepted', 'Einfahces Angebot', 0, '2019-11-14 11:44:37', 9),
(56, 'accepted', 'Einfahces Angebot', 0, '2019-11-14 11:44:37', 9),
(58, 'accepted', 'Erwreitertes Angebot', 0, '2019-11-14 18:50:48', 9),
(59, 'accepted', 'Erwreitertes Angebot', 0, '2019-11-14 18:53:09', 9),
(62, 'draft', '3rd', 0, '2019-11-17 16:54:53', 4),
(63, 'accepted', 'Erstes Angebot', 1, '2020-02-16 18:18:13', 13),
(64, 'accepted', 'Zweites Angebot', 0, '2020-02-16 18:19:20', 13),
(66, 'accepted', 'Einfahces Angebot', 0, '2020-02-16 18:24:42', 9);

-- --------------------------------------------------------

--
-- Table structure for table `Tasks`
--

CREATE TABLE `Tasks` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `title` text NOT NULL,
  `mins` int(11) NOT NULL COMMENT 'geplante minuten',
  `euros` int(11) NOT NULL COMMENT 'geplante euros',
  `description` text NOT NULL,
  `iss` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deadline` datetime NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'erstellt von',
  `assignee_uid` int(11) NOT NULL COMMENT 'verantwortlicher',
  `checked` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Tasks`
--

INSERT INTO `Tasks` (`id`, `pid`, `title`, `mins`, `euros`, `description`, `iss`, `deadline`, `uid`, `assignee_uid`, `checked`) VALUES
(3, 4, 'test2 für fdasf', 120, 0, 'bla', '2019-11-12 14:09:25', '2019-11-14 14:09:25', 6, 30, 1),
(4, 4, 'Test2', 50, 33, 'Bla', '2019-11-12 14:18:17', '1970-01-01 01:00:00', 6, 6, 1),
(5, 9, 'Testaufgabe', 5, 50, '', '2019-11-15 14:09:53', '2019-11-22 14:09:53', 34, 0, 1),
(9, 9, 'Test', 0, 0, '', '2019-11-17 16:22:37', '2019-11-24 16:22:37', 6, 0, 1),
(10, 9, 'Test', 0, 0, '', '2019-11-17 16:22:37', '2019-11-24 16:22:37', 6, 0, 1),
(11, 4, 'fghjkl', 57, 38, '', '2019-11-17 17:01:46', '2019-11-24 17:01:46', 6, 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `pass` text NOT NULL,
  `email` text NOT NULL,
  `policies` json NOT NULL,
  `wage` float NOT NULL DEFAULT '16' COMMENT 'standartlohn in euro pro stunde',
  `bankdetails` tinyblob NOT NULL COMMENT 'bank details'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`id`, `fname`, `lname`, `pass`, `email`, `policies`, `wage`, `bankdetails`) VALUES
(1, 'Dominik', 'Ziegenhagel', '188e7cfb407993938395d5a7679461cbb86ca8e1d764828eec66595c084df2017faacc3320295f0c9e91e13d6df9203e17c311813c0bae2e62f511803ebee6a4', 'info@ziegenhagel.com', '{\"hours\": [\"list\", \"create\", \"edit\", \"delete\"], \"tasks\": [\"create\", \"edit\", \"delete\", \"list\"], \"users\": [\"create\", \"list\", \"edit\", \"delete\"], \"wages\": [\"list\", \"delete\", \"create\", \"edit\"], \"brutto\": [\"list\", \"create\", \"edit\", \"delete\"], \"quotes\": [\"list\", \"edit\", \"create\", \"delete\"], \"invoices\": [\"list\", \"edit\", \"create\", \"delete\"], \"policies\": [\"edit\", \"list\"], \"projects\": [\"listall\", \"create\", \"list\", \"edit\", \"delete\"], \"customers\": [\"create\", \"list\", \"edit\", \"delete\"]}', 46, 0x7b224942414e223a2244453236313030313130303132363239383832313634222c22424943223a224e54534244454231585858227d),
(34, 'Paul', 'Weiß', 'f1404d4a5d7e9823fa7f1dbe8c666ea920c06e4fb98199980c5b13d38f511e8f36c12d88b369a64acc999bd2063fd92f17d35ab22e29ef5dbba2f9c73f683b14', 'paul@covomedia.com', '{\"hours\": [\"list\", \"edit\", \"create\", \"delete\"], \"tasks\": [\"create\", \"list\"], \"users\": [\"list\"], \"quotes\": [\"list\"], \"invoices\": [\"list\", \"create\"], \"projects\": [\"list\", \"edit\", \"create\"], \"customers\": [\"create\", \"edit\", \"list\"]}', 46, 0x7b224942414e223a2244453236313030313130303132363239383832313634222c22424943223a224e54534244454231585858227d),
(35, 'Ulrike', 'Ziegenhagel', 'fb83acb4370e78cf3d578280383cc66ae8260244fe732e968192ba90541a179cc35eb0fbee718a7655e0dc1a4047e02185dda2b111f0b587d9496a50e93f059b', 'ulrikeziegenhagel@gmx.de', '{\"hours\": [\"list\", \"create\"], \"tasks\": [\"list\"], \"users\": [\"list\"], \"wages\": [\"list\"], \"brutto\": [\"list\", \"create\", \"edit\", \"delete\"], \"quotes\": [\"list\"], \"invoices\": [\"list\", \"edit\", \"create\", \"delete\"], \"projects\": [\"listall\", \"list\"], \"customers\": [\"list\", \"edit\"]}', 46, 0x7b224942414e223a2244453236313030313130303132363239383832313634222c22424943223a224e54534244454231585858227d);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Customers`
--
ALTER TABLE `Customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Expenses`
--
ALTER TABLE `Expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Files`
--
ALTER TABLE `Files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Hours`
--
ALTER TABLE `Hours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `InvoiceHasHours`
--
ALTER TABLE `InvoiceHasHours`
  ADD PRIMARY KEY (`invoice_id`,`hours_id`);

--
-- Indexes for table `Invoices`
--
ALTER TABLE `Invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Logins`
--
ALTER TABLE `Logins`
  ADD PRIMARY KEY (`hash`);

--
-- Indexes for table `Policies`
--
ALTER TABLE `Policies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ProjectHasWage`
--
ALTER TABLE `ProjectHasWage`
  ADD PRIMARY KEY (`pid`,`uid`);

--
-- Indexes for table `Projects`
--
ALTER TABLE `Projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `QuoteHasFixed`
--
ALTER TABLE `QuoteHasFixed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Quotes`
--
ALTER TABLE `Quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Tasks`
--
ALTER TABLE `Tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Customers`
--
ALTER TABLE `Customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `Files`
--
ALTER TABLE `Files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Hours`
--
ALTER TABLE `Hours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `Invoices`
--
ALTER TABLE `Invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'rechnungsnr zb 1902 (2019 2nd invoice)', AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `Policies`
--
ALTER TABLE `Policies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Projects`
--
ALTER TABLE `Projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `QuoteHasFixed`
--
ALTER TABLE `QuoteHasFixed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT for table `Quotes`
--
ALTER TABLE `Quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `Tasks`
--
ALTER TABLE `Tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
