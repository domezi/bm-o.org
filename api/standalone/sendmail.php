<?php
$success=false;
if($_POST["to"]=="__main" && $_POST["subject"]=="") {
	unset($_POST["subject"]);
	unset($_POST["to"]);
	$empfaenger = 'info@covomedia.com';
	$betreff = "Covomedia Kontakt-Formular vom ".date("d.m H:i");
	$nachricht = json_encode($_POST);
	$success = mail($empfaenger, $betreff, $nachricht);
}
if($success) {
	header("location: https://www.covomedia.com/kontakt/danke");
} else {
	header("location: https://www.covomedia.com/kontakt/sorry");
}
