<?php

require("../dbcon.php");

header('Content-Type: application/json');

$update_response = file_get_contents("php://input");
$update_response = json_decode($update_response, true);
file_put_contents("request.json",json_encode($update_response));

function duration_to_mins($in) {
	// takes googles unit paramater array as input and returns int minutes
	if($in["unit"] === "stunde" || $in["unit"] === "stunden")
		return $in["amount"] * 60; // interpret as hours
	return $in["amount"] ;
}

function final_words($fulfillmentText) {
	// dies outputting the following text to user
	$out=array("fulfillmentText"=>$fulfillmentText);
	die(json_encode($out));
}

$update_response = file_get_contents("php://input");
$update_response = json_decode($update_response, true);
file_put_contents("request.json",json_encode($update_response));

$action = $update_response["queryResult"]["action"];
$action = $update_response["queryResult"]["intent"]["displayName"];
$params = $update_response["queryResult"]["parameters"];
//$params = $update_response["queryResult"]["outputContexts"]["parameters"];
$allRequiredParamsPresent = $update_response["queryResult"]["allRequiredParamsPresent"];

$out=array("fulfillmentText"=>"action: ".$action);

switch ($action) {
    case "invoices.list":

        $out["fulfillmentText"]="Hier sind die aktuellen Rechnungen:\n1) Testrechnung\n2) Alte Testrechnung\n3) Bezahlte Altrechnung\n\nBitte geben Sie eine Zahl zwischen 1 und 3 ein:";

	$card = array(
		"imageUri" => "https://www.carverartsandscience.org/sites/main/files/imagecache/carousel/main-images/camera_lense_0.jpeg",
		"title" => "card title",
		"subtitle" => "card text"
	);

	$card["buttons"][] = array("text"=>"Button text","postback"=>"yes");
	$card["buttons"][] = array("text"=>"Button text","postback"=>"yes");
	$card["buttons"][] = array("text"=>"Button text","postback"=>"yes");

	$out["fulfillmentMessages"][]=array("card"=>$card);

        break;
    case "hours.create":
	
	// calculate
	
	$project_res = $db->query("select * from Projects where slug like '".$params["project"]."' and status != 'trash' union select * from Projects where title like '".$params["project"]."%' and status != 'trash' limit 1");
	if(!$project_res || !$project_res->num_rows) {
		if(strlen($params["project"])>0)
			final_words("Ich konnte keine Projekt mit dem SLUG \"".$params["project"]."\" finden");
		else
			final_words("Ich konnnte den Projektnamen nicht erkennen.");
	}
	$project = $project_res->fetch_object();
	$mins = duration_to_mins($params["duration"]);

	// return to user

        $out["fulfillmentText"]="Okay, darf ich folgende Arbeitszeit anlegen?\r\n"."Projekt: ".$project->title.", Zeit: ".	$mins."min, Beschreibung: ".$params["description"];

	$card = array(
		"title" => "Arbeitszeit anlegen",
		"subtitle" => "Projekt: ".$project->title.", Zeit: ".	$mins."min, Beschreibung: ".$params["description"]
	);

	$card["buttons"][] = array("text"=>"Anlegen","postback"=>"ja");
	$card["buttons"][] = array("text"=>"Abbrechen","postback"=>"nein");

	$out["fulfillmentMessages"][]=array("card"=>$card);

        break;
    case 2:
        echo "i ist gleich 2";
        break;
}

die(json_encode($out));

