<?php
require("dbcon.php");
require("libs.php");

//get pid
$pid = 0;
$description = "Details auf Slack";
$project = null;

$uid = $db->query("select * from Users where slack_id = '".$_POST["user_id"]."'");
if($uid->num_rows>0) {
	$uid = $uid->fetch_object()->id;
} else {
	die(":lock: Du musst Deinen Overtime Account zuerst mit Slack verknüpfen.\r\nGehe Dazu in Overtime unter https://overtime.covomedia.com/#/settings und gib als Slack ID *".$_POST["user_id"]."* ein.");
}

$parts = explode(" ",$_POST["text"]);
$res = $db->query("select id, title from Projects where slug like '".$parts[0]."'");
if(strlen($parts[0])>1 && $res->num_rows > 0) {
	$project=$res->fetch_object();
	$pid = $project->id;
	$description = substr($_POST["text"],strlen($parts[0])+1);
} else {
	$res = $db->query("select id, title from Projects where slug like '".$_POST["channel_name"]."'");
	if($res->num_rows > 0) {
		$project=$res->fetch_object();
		$pid = $project->id;
		$description = $_POST["text"];
	}
}

if($pid === 0) {
	die("Projekt mit dem Slug \"".$parts[0]."\" bzw \"".$_POST["channel_name"]."\" wurde nicht gefunden, bitte schreibe /start [project slug] [task description]");
}
$res=$db->query("select * from ProjectHasWage where uid = '".$uid."' and pid = '".$pid."'");
if($res->num_rows === 0) {
	die("Du hast auf Overtime nicht die Berechtigungen, Arbeitszeiten im Projekt \"".$project->title."\" zu erfassen. Lass Dir von Dominik die entsprechenden Rechte geben.");
}

if(strlen($description)<=1) {
	$description = "Details auf Slack";
}

if($_POST["command"] === "/start") {
	$res=$db->query("select * from Hours where pid='".$pid."' and uid = '".$uid."' and tid = '-1' limit 1");
	if($res->num_rows) {
		die("Du hast bereits eine Arbeitszeitenerfassung gestartet. Beende sie mit /discard oder /done.");
	}
	$db->query("INSERT INTO `Hours` (`id`, `text`, `uid`, `pid`, `iss`, `mins`, `euros`, `tid`) VALUES (NULL, '".ucfirst($description)."', '".$uid."', '".$pid."', CURRENT_TIMESTAMP, '0', '0', '-1')");
	echo ":timer_clock: Arbeitszeit in \"".$project->title."\" wurde begonnen. Beende die Arbeitszeit mit /done";
} else if($_POST["command"] === "/discard"||$_POST["command"] === "/done") {
	$res=$db->query("select * from Hours where pid='".$pid."' and uid = '".$uid."' and tid = '-1' limit 1");
	if($res->num_rows) $hour = $res->fetch_object(); else die(":no_entry_sign: Nichts zu tun. Es wurden keine aktiven Arbeitszeiten für \"".$project->title."\" gefunden. Starte Arbeitszeiten mit /start.");
	$mins = (time()-strtotime($hour->iss))/60;
	$res=$db->query("select wage from ProjectHasWage where uid = '".$uid."' and pid = '".$pid."'");
	$euros = $mins * $res->fetch_object()->wage;

	if($_POST["command"] === "/done") {
		$db->query("delete from `Hours` where id = '".$hour->id."'");
		echo ":white_check_mark: Aktive Arbeitszeit für \"".$project->title."\" wurde gelöscht. Gelöschte Gesamtzeit: ".intval($mins)."min (".euros($euros)."€)";
	} else {
		$db->query("update `Hours` set mins = '".$mins."', euros = '".$euros."',tid = '0' where id = '".$hour->id."'");
		echo ":wastebasket: Arbeitszeit für \"".$project->title."\" wurde beendet. Gesamtzeit: ~".intval($mins)."min (".euros($euros)."€)";
	}
}


