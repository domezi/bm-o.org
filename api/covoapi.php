<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Credentials: true');
header("Content-Type: application/json");

require("config.php");
require("dbcon.php");
require("libs.php");


define(ROUTE, explode("/", substr($_SERVER["REQUEST_URI"], 1)));
define(METHOD, strtoupper($_SERVER["REQUEST_METHOD"]));

// prepare body
$body = json_decode(file_get_contents("php://input"), true);
foreach ($body as $key => $val) {
	if (is_string($val))
		$body[$key] = str_replace("'", "", str_replace("%", "", $val));
}
// handle routing
//$out = [];

$subroute = -1;
if (is_file("routes/" . ROUTE[0] . "/" . ROUTE[2] . "/" . ROUTE[4] . ".php")) {
	$subroute = 2;
} else if (is_file("routes/" . ROUTE[0] . "/" . ROUTE[2] . ".php")) {
	$subroute = 1;
} else if (is_file("routes/" . ROUTE[0] . ".php")) {
	$subroute = 0;
}

$hash = str_replace("Bearer ", "", $_SERVER["REDIRECT_HTTP_AUTHORIZATION"]);
$res = $db->query("select l.uid,l.server from Logins l, Users u where l.uid = u.id and l.hash = '" . $hash . "' and l.iss > '".date("Y-m-d H:i:s",strtotime("-2 days"))."'");
if (ROUTE[0] !== "auth" && $res->num_rows > 0) {
	$user=$res->fetch_object();
	$server = json_decode($user->server,0);
	if($server->REMOTE_ADDR !== $_SERVER["REMOTE_ADDR"])
		die(json_encode(array("error"=>"ip_changed","server"=>$server)));
	else
		define(UID, intval($user->uid));
}
else
	define(UID, -1);

$out = array("uid" => UID, "route" => ROUTE, "method" => METHOD, "remote_addr"=>$_SERVER["REMOTE_ADDR"]);

if ($subroute >= 0) {
	if (UID == -1 && ROUTE[0] != "auth")
		done(403);

	// do route
	if ($subroute==2)
		include("routes/" . ROUTE[0] . "/" . ROUTE[2] . "/" . ROUTE[4] . ".php");
	else if ($subroute==1)
		include("routes/" . ROUTE[0] . "/" . ROUTE[2] . ".php");
	else
		include("routes/" . ROUTE[0] . ".php");

	if (isset($success)) $out["success"] = boolval($success);
	if (isset($data)) $out["data"] = $data;
	if (isset($success) && !isset($data)) done($success ? 200 : 500);
} else {
	$out["error"] = "route not found";
}

// output and finish
die(json_encode($out));
